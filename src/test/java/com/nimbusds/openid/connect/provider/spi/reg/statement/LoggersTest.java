package com.nimbusds.openid.connect.provider.spi.reg.statement;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class LoggersTest {
	

	@Test
	public void testLoggerNames() {
		
		assertEquals("MAIN", Loggers.MAIN.getName());
		assertEquals("CLIENT-REG", Loggers.REGISTRATION.getName());
	}
}
