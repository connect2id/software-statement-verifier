package com.nimbusds.openid.connect.provider.spi.reg.statement;


import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.List;

import static net.jadler.Jadler.*;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.KeySourceException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jose.util.DefaultResourceRetriever;
import com.nimbusds.jwt.JWTClaimsSet;


public class SoftwareStatementBasedKeySelectorTest {
	
	
	@Before
	public void setUp() {
		initJadler();
	}
	
	
	@After
	public void tearDown() {
		closeJadler();
	}
	
	
	private void publishJWKSet(final JWK jwk) {
		
		var jwkSet = new JWKSet(jwk);
		
		onRequest()
			.havingMethodEqualTo("GET")
			.havingPathEqualTo("/jwks.json")
			.respond()
			.withStatus(200)
			.withBody(jwkSet.toPublicJWKSet().toString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");
	}


	@Test
	public void testSelectKey()
		throws URISyntaxException, JOSEException {
		
		var rsaJWK = new RSAKeyGenerator(2048)
			.keyUse(KeyUse.SIGNATURE)
			.algorithm(JWSAlgorithm.RS256)
			.keyID("1")
			.generate();
		
		publishJWKSet(rsaJWK);
	
		var claimName = "org_jwks_endpoint";
		
		var selector = new SoftwareStatementBasedKeySelector(
			claimName,
			new DefaultResourceRetriever());
		
		var uri = URI.create("http://localhost:" + port() + "/jwks.json");
		
		var statementClaims = new JWTClaimsSet.Builder()
			.issuer("https://publisher.example.com")
			.claim("software_id", "65d1f27c-4aea-4549-9c21-60e495a7a86f")
			.claim("org_jwks_endpoint", uri.toString())
			.build();
		
		assertEquals(uri, selector.resolveJWKSetURL(statementClaims).toURI());
		
		List<Key> candidates = selector.selectJWSKeys(
			new JWSHeader(JWSAlgorithm.RS256),
			new SoftwareStatementContext(statementClaims));
		
		assertEquals(1, candidates.size());
		
		assertArrayEquals(rsaJWK.toPublicKey().getEncoded(), candidates.get(0).getEncoded());
	}


	@Test
	public void testSelectKey_noKeyID_noKeyUse_noAlg()
		throws URISyntaxException, JOSEException {
		
		var rsaJWK = new RSAKeyGenerator(2048)
			.generate();
		
		publishJWKSet(rsaJWK);
	
		var claimName = "org_jwks_endpoint";
		
		var selector = new SoftwareStatementBasedKeySelector(
			claimName,
			new DefaultResourceRetriever());
		
		var uri = URI.create("http://localhost:" + port() + "/jwks.json");
		
		var statementClaims = new JWTClaimsSet.Builder()
			.issuer("https://publisher.example.com")
			.claim("software_id", "65d1f27c-4aea-4549-9c21-60e495a7a86f")
			.claim("org_jwks_endpoint", uri.toString())
			.build();
		
		assertEquals(uri, selector.resolveJWKSetURL(statementClaims).toURI());
		
		List<Key> candidates = selector.selectJWSKeys(
			new JWSHeader(JWSAlgorithm.RS256),
			new SoftwareStatementContext(statementClaims));
		
		assertEquals(1, candidates.size());
		
		assertArrayEquals(rsaJWK.toPublicKey().getEncoded(), candidates.get(0).getEncoded());
	}


	@Test
	public void testSelectKey_encryptionKey()
		throws URISyntaxException, JOSEException {
		
		var rsaJWK = new RSAKeyGenerator(2048)
			.keyUse(KeyUse.ENCRYPTION)
			.generate();
		
		publishJWKSet(rsaJWK);
	
		var claimName = "org_jwks_endpoint";
		
		var selector = new SoftwareStatementBasedKeySelector(
			claimName,
			new DefaultResourceRetriever());
		
		var uri = URI.create("http://localhost:" + port() + "/jwks.json");
		
		var statementClaims = new JWTClaimsSet.Builder()
			.issuer("https://publisher.example.com")
			.claim("software_id", "65d1f27c-4aea-4549-9c21-60e495a7a86f")
			.claim("org_jwks_endpoint", uri.toString())
			.build();
		
		assertEquals(uri, selector.resolveJWKSetURL(statementClaims).toURI());
		
		List<Key> candidates = selector.selectJWSKeys(
			new JWSHeader(JWSAlgorithm.RS256),
			new SoftwareStatementContext(statementClaims));
		
		assertTrue(candidates.isEmpty());
	}


	@Test
	public void testSelectKey_404()
		throws URISyntaxException {
	
		var claimName = "org_jwks_endpoint";
		
		var selector = new SoftwareStatementBasedKeySelector(
			claimName,
			new DefaultResourceRetriever());
		
		var uri = URI.create("http://localhost:" + port() + "/jwks.json");
		
		var statementClaims = new JWTClaimsSet.Builder()
			.issuer("https://publisher.example.com")
			.claim("software_id", "65d1f27c-4aea-4549-9c21-60e495a7a86f")
			.claim("org_jwks_endpoint", uri.toString())
			.build();
		
		assertEquals(uri, selector.resolveJWKSetURL(statementClaims).toURI());
		
		try {
			selector.selectJWSKeys(
				new JWSHeader(JWSAlgorithm.RS256),
				new SoftwareStatementContext(statementClaims));
			fail();
		} catch (KeySourceException e) {
			assertEquals("Couldn't retrieve remote JWK set: http://localhost:" + port() + "/jwks.json", e.getMessage());
		}
	}
}
