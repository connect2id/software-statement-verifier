package com.nimbusds.openid.connect.provider.spi.reg.statement;


import java.io.File;
import java.net.URI;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import static org.junit.Assert.*;

import com.thetransactioncompany.json.pretty.PrettyJson;
import org.junit.After;
import org.junit.Test;

import com.nimbusds.common.contenttype.ContentType;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jose.util.IOUtils;
import com.nimbusds.jose.util.X509CertUtils;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.util.DateUtils;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.ClientAuthenticationMethod;
import com.nimbusds.oauth2.sdk.auth.PrivateKeyJWT;
import com.nimbusds.oauth2.sdk.auth.SelfSignedTLSClientAuthentication;
import com.nimbusds.oauth2.sdk.auth.X509CertificateConfirmation;
import com.nimbusds.oauth2.sdk.client.ClientInformation;
import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationRequest;
import com.nimbusds.oauth2.sdk.client.ClientRegistrationResponse;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.SoftwareID;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;


public class SampleOpenBankingStyleFlowTest {
	
	
	static final Issuer OP_ISSUER = new Issuer("https://demo.c2id.com");
	
	
	static final URI OP_CLIENTS_ENDPOINT = URI.create("https://demo.c2id.com/clients/");
	
	
	static final URI OP_TOKEN_ENDPOINT = URI.create("https://demo.c2id.com/token/");
	
	
	static final URI OP_TOKEN_INTROSPECTION_ENDPOINT = URI.create("https://demo.c2id.com/token/introspect");
	
	
	static final Issuer PUBLISHER_ISSUER = new Issuer("https://publisher.example.com");
	
	
	static final JWKSet PUBLISHER_PRIVATE_JWK_SET;
	
	
	static final URI PUBLISHER_JWK_SET_URL = URI.create("https://c2id-test.s3.eu-west-2.amazonaws.com/ssv/sample-publisher-jwks.json");
	
	
	static final Issuer CLIENT_ISSUER = new Issuer("https://client.example.com");
	
	
	static final JWKSet CLIENT_PRIVATE_JWK_SET;
	
	
	static final URI CLIENT_JWK_SET_URL = URI.create("https://c2id-test.s3.eu-west-2.amazonaws.com/ssv/sample-client-jwks.json");
	
	
	static final X509Certificate CLIENT_CERTIFICATE;
	
	
	static final String CLIENT_NAME = "My Banking App";
	
	
	static final String CLIENT_ORG_ID = "org-10444";
	
	
	static final String CLIENT_ORG_NAME = "Client Org Ltd.";
	
	
	static final SoftwareID CLIENT_SOFTWARE_ID = new SoftwareID("eb985c63-2280-4889-898d-d9d9b3ddc74f");
	
	
	static final SSLContext CLIENT_SSL_CONTEXT;
	
	
	static final JWKSet RS_PRIVATE_JWK_SET;
	
	
	static final PrettyJson PRETTY_JSON = new PrettyJson(PrettyJson.Style.DEFAULT);
	
	
	static {
		
		try {
			PUBLISHER_PRIVATE_JWK_SET = JWKSet.load(new File("src/test/resources/sample-publisher-private-jwks.json"));
			
			CLIENT_PRIVATE_JWK_SET = JWKSet.load(new File("src/test/resources/sample-client-private-jwks.json"));
			
			CLIENT_CERTIFICATE = X509CertUtils.parseWithException(IOUtils.readFileToString(new File("src/test/resources/sample-client-certificate.pem")));
			
			// Store the private key together with the certificate
			var ks = KeyStore.getInstance("JKS");
			ks.load(null); // init
			ks.setKeyEntry("client-auth", ((RSAKey)CLIENT_PRIVATE_JWK_SET.getKeys().get(0)).toPrivateKey(), new char[0], new Certificate[]{CLIENT_CERTIFICATE});

			// Key manager factory for the SSL context
			var kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, new char[0]);

			// Trust manager factory for the SSL context
			var tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init((KeyStore) null); // null here initialises the TMF with the default trust store.

			// Create a new SSL context
			CLIENT_SSL_CONTEXT = SSLContext.getInstance("TLS");
			CLIENT_SSL_CONTEXT.init(kmf.getKeyManagers(), tmf.getTrustManagers(), new SecureRandom());
			
			var rsJWK = new RSAKeyGenerator(2048)
				.keyIDFromThumbprint(true)
				.generate();
			
			RS_PRIVATE_JWK_SET = new JWKSet(rsJWK);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	static SignedJWT createSoftwareStatement()
		throws Exception {
		
		var claims = new JWTClaimsSet.Builder()
			.issuer(PUBLISHER_ISSUER.getValue())
			.issueTime(new Date())
			.jwtID(UUID.randomUUID().toString())
			.claim("software_id", CLIENT_SOFTWARE_ID.getValue())
			.claim("software_client_id", "123")
			.claim("software_client_name", CLIENT_NAME)
			.claim("software_client_uri", CLIENT_ISSUER.getValue())
			.claim("software_version", "1.0")
			.claim("software_jwks_endpoint", CLIENT_JWK_SET_URL.toString())
			.claim("org_status", "Active")
			.claim("org_id", CLIENT_ORG_ID)
			.claim("org_name", CLIENT_ORG_NAME)
			.build();
		
		var header = new JWSHeader.Builder(JWSAlgorithm.PS256)
			.type(JOSEObjectType.JWT)
			.keyID(PUBLISHER_PRIVATE_JWK_SET.getKeys().get(0).getKeyID())
			.build();
		
		var softwareStatement = new SignedJWT(header, claims);
		
		softwareStatement.sign(new RSASSASigner((RSAKey) PUBLISHER_PRIVATE_JWK_SET.getKeys().get(0)));
		
		System.out.println("Created software statement: ");
		System.out.println(softwareStatement.serialize());
		System.out.println("Header: " + PRETTY_JSON.format(softwareStatement.getHeader().toJSONObject()));
		System.out.println("Claims: " + PRETTY_JSON.format(softwareStatement.getJWTClaimsSet().toJSONObject()));
		
		return softwareStatement;
	}
	
	
	static ClientMetadata createClientMetadata(final SignedJWT softwareStatement) {
		
		var clientMetadata = new ClientMetadata();
		clientMetadata.setSoftwareStatement(softwareStatement);
		clientMetadata.setGrantTypes(Set.of(GrantType.CLIENT_CREDENTIALS));
		clientMetadata.setTokenEndpointAuthMethod(ClientAuthenticationMethod.SELF_SIGNED_TLS_CLIENT_AUTH);
		clientMetadata.setJWKSetURI(CLIENT_JWK_SET_URL);
		clientMetadata.setScope(new Scope("payments"));
		
		System.out.println("Client metadata: ");
		System.out.println(PRETTY_JSON.format(clientMetadata.toJSONObject()));
		
		return clientMetadata;
	}
	
	
	static SignedJWT createRegistrationRequestJWT(final ClientMetadata clientMetadata)
		throws Exception {
		
		var now = new Date();
		
		var claims = new JWTClaimsSet.Builder(JWTClaimsSet.parse(clientMetadata.toJSONObject()))
			.issuer(CLIENT_ISSUER.getValue())
			.issueTime(now)
			.expirationTime(DateUtils.fromSecondsSinceEpoch(now.getTime() + 600))
			.audience(OP_ISSUER.getValue())
			.jwtID(UUID.randomUUID().toString())
			.build();
		
		var header = new JWSHeader.Builder(JWSAlgorithm.PS256)
			.type(JOSEObjectType.JWT)
			.keyID(CLIENT_PRIVATE_JWK_SET.getKeys().get(0).getKeyID())
			.build();
		
		var requestJWT = new SignedJWT(header, claims);
		
		requestJWT.sign(new RSASSASigner((RSAKey) CLIENT_PRIVATE_JWK_SET.getKeys().get(0)));
		
		System.out.println("Created request JWT: ");
		System.out.println(requestJWT.serialize());
		System.out.println("Header: " + PRETTY_JSON.format(requestJWT.getHeader().toJSONObject()));
		System.out.println("Claims: " + PRETTY_JSON.format(requestJWT.getJWTClaimsSet().toJSONObject()));
		
		return requestJWT;
	}
	
	
	static ClientInformation registerClient(final SignedJWT requestJWT)
		throws Exception {
		
		System.setProperty("javax.net.debug", "all");
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, OP_CLIENTS_ENDPOINT.toURL());
		httpRequest.setSSLSocketFactory(CLIENT_SSL_CONTEXT.getSocketFactory());
		httpRequest.setEntityContentType(ContentType.APPLICATION_JWT);
		httpRequest.setQuery(requestJWT.serialize());
		
		var httpResponse = httpRequest.send();
		
		var response = ClientRegistrationResponse.parse(httpResponse);
		
		if (! response.indicatesSuccess()) {
			fail(httpResponse.getStatusCode() + " " + httpResponse.getStatusMessage() + "\n" + httpResponse.getContent());
		}
		
		var clientInfo = response.toSuccessResponse().getClientInformation();
		
		System.out.println("Registered client: ");
		System.out.println(PRETTY_JSON.format(clientInfo.toJSONObject()));
		return clientInfo;
	}
	
	
	static ClientInformation registerResourceServerAsClient()
		throws Exception {
		
		var clientMetadata = new ClientMetadata();
		clientMetadata.setGrantTypes(Set.of());
		clientMetadata.setResponseTypes(Set.of());
		clientMetadata.setTokenEndpointAuthMethod(ClientAuthenticationMethod.PRIVATE_KEY_JWT);
		clientMetadata.setTokenEndpointAuthJWSAlg(JWSAlgorithm.PS256);
		clientMetadata.setJWKSet(RS_PRIVATE_JWK_SET.toPublicJWKSet());
		clientMetadata.setScope(new Scope(OP_TOKEN_INTROSPECTION_ENDPOINT.toString()));
		
		var httpRequest = new ClientRegistrationRequest(
			OP_CLIENTS_ENDPOINT,
			clientMetadata,
			new BearerAccessToken("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6"))
			.toHTTPRequest();
			
		var httpResponse = httpRequest.send();
		
		var response = ClientRegistrationResponse.parse(httpResponse);
		
		if (! response.indicatesSuccess()) {
			fail(httpResponse.getStatusCode() + " " + httpResponse.getStatusMessage() + "\n" + httpResponse.getContent());
		}
		
		var clientInfo = response.toSuccessResponse().getClientInformation();
		
		System.out.println("Registered RS as client: ");
		System.out.println(PRETTY_JSON.format(clientInfo.toJSONObject()));
		return clientInfo;
	}
	
	
	static AccessToken requestAccessToken(final ClientID clientID)
		throws Exception {
		
		var tokenRequest = new TokenRequest(
			OP_TOKEN_ENDPOINT,
			new SelfSignedTLSClientAuthentication(clientID, CLIENT_CERTIFICATE),
			new ClientCredentialsGrant(),
			new Scope("payments")
		);
		
		var httpRequest = tokenRequest.toHTTPRequest();
		httpRequest.setSSLSocketFactory(CLIENT_SSL_CONTEXT.getSocketFactory());
		
		var httpResponse = httpRequest.send();
		
		var response = TokenResponse.parse(httpResponse);
		
		if (! response.indicatesSuccess()) {
			fail(response.toErrorResponse().getErrorObject().toString());
		}
		
		var token = response.toSuccessResponse().getTokens().getAccessToken();
		
		System.out.println("Client obtained access token from Connect2id server: " + token);
		
		return token;
	}
	
	
	static TokenIntrospectionSuccessResponse introspectAccessToken(final AccessToken accessToken,
								       final ClientInformation rsClientInfo)
		throws Exception {
		
		var rsaJWK = (RSAKey)RS_PRIVATE_JWK_SET.getKeys().get(0);
		
		var request = new TokenIntrospectionRequest(
			OP_TOKEN_INTROSPECTION_ENDPOINT,
			new PrivateKeyJWT(
				rsClientInfo.getID(),
				OP_TOKEN_INTROSPECTION_ENDPOINT,
				rsClientInfo.getMetadata().getTokenEndpointAuthJWSAlg(),
				rsaJWK.toRSAPrivateKey(),
				rsaJWK.getKeyID(),
				null),
			accessToken);
			
		var httpRequest = request.toHTTPRequest();
		
		var httpResponse = httpRequest.send();
		
		var response = TokenIntrospectionResponse.parse(httpResponse);
		
		if (! response.indicatesSuccess()) {
			fail(response.toErrorResponse().getErrorObject().toString());
		}
		
		var introspection = response.toSuccessResponse();
		
		System.out.println("Token introspection: ");
		System.out.println(PRETTY_JSON.format(introspection.toJSONObject()));
		
		return introspection;
	}
	
	
	@After
	public void tearDown() {
		
		System.clearProperty("javax.net.debug");
	}


//	@Test
	public void testFlow()
		throws Exception {
		
		// The resource (web API) server must be registered as client
		// with the Connect2id server so that it can validate tokens
		// at the token introspection endpoint
		var rsClientInfo = registerResourceServerAsClient();
		
		// The software statement about the client, asserted by a
		// publisher / central directory
		var softwareStatement = createSoftwareStatement();
		
		// The client constructs its metadata around the software
		// statement
		var clientMetadata = createClientMetadata(softwareStatement);
		
		// The client registration request is a signed JWT
		var requestJWT = createRegistrationRequestJWT(clientMetadata);
		
		// Register the client with the Connect2id server, the client
		// certificate, request JWT and software statements get
		// validated internally before allowing the registration to
		// proceed
		var clientInfo = registerClient(requestJWT);
		
		// The client requests an access token using the OAuth 2.0
		// client credentials (service-to-service) grant
		var token = requestAccessToken(clientInfo.getID());
		
		// The resource server introspects the access token, which is
		// client certificate bound
		var introspection = introspectAccessToken(token, rsClientInfo);
		
		assertTrue(introspection.isActive());
		assertTrue(introspection.getScope().contains(new Scope.Value("payments")));
		assertEquals(CLIENT_ORG_ID, introspection.getJSONObjectParameter("dat").get("obtppatm_orgId"));
		assertEquals(X509CertificateConfirmation.of(CLIENT_CERTIFICATE), introspection.getX509CertificateConfirmation());
	}
}
