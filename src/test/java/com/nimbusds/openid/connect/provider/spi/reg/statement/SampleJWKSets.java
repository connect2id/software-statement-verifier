package com.nimbusds.openid.connect.provider.spi.reg.statement;


import java.io.FileWriter;
import java.security.KeyPairGenerator;
import java.security.interfaces.RSAPublicKey;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import com.thetransactioncompany.json.pretty.PrettyJson;
import org.junit.Test;

import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.KeyUse;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.X509CertUtils;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.X509CertificateUtils;


public class SampleJWKSets {
	
	
	static final Issuer PUBLISHER_ISSUER = new Issuer("https://publisher.example.com");
	
	
	static final Issuer CLIENT_ISSUER = new Issuer("https://client.example.com");
	
	
	static final PrettyJson PRETTY_JSON = new PrettyJson(PrettyJson.Style.DEFAULT);
	
	
//	@Test
	public void generate()
		throws Exception {
		
		var now = Instant.now();
		
		var rsaGenerator = KeyPairGenerator.getInstance("RSA");
		rsaGenerator.initialize(2048);
		
		// publisher
		var keyPair = rsaGenerator.generateKeyPair();
		
		var publisherCert = X509CertificateUtils.generateSelfSigned(
			PUBLISHER_ISSUER,
			Date.from(now),
			Date.from(now.plus(365, ChronoUnit.DAYS)),
			keyPair.getPublic(),
			keyPair.getPrivate());
		
		var fw = new FileWriter("src/test/resources/sample-publisher-certificate.pem");
		fw.write(X509CertUtils.toPEMString(publisherCert, true));
		fw.close();
		
		var publisherJWK = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
			.privateKey(keyPair.getPrivate())
			.keyUse(KeyUse.SIGNATURE)
			.algorithm(JWSAlgorithm.PS256)
			.keyID(X509CertUtils.computeSHA256Thumbprint(publisherCert).toString())
			.x509CertChain(List.of(Base64.encode(publisherCert.getEncoded())))
			.build();
		
		var publisherJWKSet = new JWKSet(publisherJWK);
		
		fw = new FileWriter("src/test/resources/sample-publisher-private-jwks.json");
		fw.write(PRETTY_JSON.format(publisherJWKSet.toJSONObject(false)));
		fw.close();
		
		fw = new FileWriter("src/test/resources/sample-publisher-jwks.json");
		fw.write(PRETTY_JSON.format(publisherJWKSet.toPublicJWKSet().toJSONObject()));
		fw.close();
		
		// client
		keyPair = rsaGenerator.generateKeyPair();
		
		var clientCert = X509CertificateUtils.generate(
			PUBLISHER_ISSUER,
			new Subject(CLIENT_ISSUER.getValue()),
			Date.from(now),
			Date.from(now.plus(365, ChronoUnit.DAYS)),
			keyPair.getPublic(),
			publisherJWK.toPrivateKey());
		
		fw = new FileWriter("src/test/resources/sample-client-certificate.pem");
		fw.write(X509CertUtils.toPEMString(clientCert, true));
		fw.close();
		
		var clientJWK = new RSAKey.Builder((RSAPublicKey) keyPair.getPublic())
			.privateKey(keyPair.getPrivate())
			.keyUse(KeyUse.SIGNATURE)
			.algorithm(JWSAlgorithm.PS256)
			.keyID(X509CertUtils.computeSHA256Thumbprint(clientCert).toString())
			.x509CertChain(List.of(Base64.encode(clientCert.getEncoded()), Base64.encode(publisherCert.getEncoded())))
			.build();
		
		var clientJWKSet = new JWKSet(clientJWK);
		
		fw = new FileWriter("src/test/resources/sample-client-private-jwks.json");
		fw.write(PRETTY_JSON.format(clientJWKSet.toJSONObject(false)));
		fw.close();
		
		fw = new FileWriter("src/test/resources/sample-client-jwks.json");
		fw.write(PRETTY_JSON.format(clientJWKSet.toPublicJWKSet().toJSONObject()));
		fw.close();
	}
}
