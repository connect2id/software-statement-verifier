package com.nimbusds.openid.connect.provider.spi.reg.statement;


import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static org.junit.Assert.*;

import com.nimbusds.jose.JOSEObjectType;
import org.junit.Test;

import com.nimbusds.common.config.ConfigurationException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.Issuer;


public class ConfigurationTest {


	@Test
	public void testParse()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());
		
		config.log();
	}


	@Test
	public void testParse_withExplicitSoftwareStatementTyping()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.jwtTypes", "software-statement+jwt");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertEquals(Set.of(new JOSEObjectType("software-statement+jwt")), config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());
		
		config.log();
	}


	@Test
	public void testParse_withExplicitSoftwareStatementTyping_multiple()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.jwtTypes", "JWT, software-statement+jwt");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertEquals(Set.of(JOSEObjectType.JWT, new JOSEObjectType("software-statement+jwt")), config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());

		config.log();
	}


	@Test
	public void testParse_withAdditionalRequiredSoftwareStatementClaims()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.additionalRequiredClaims", "iat,jti");
		props.setProperty("op.ssv.logClaims", "iss,iat,jti");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertEquals(Set.of("iat", "jti"), config.additionalRequiredClaims);
		assertEquals(Set.of("iss", "iat", "jti"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());

		config.log();
	}


	@Test
	public void testParse_withRequireClientCertificate()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.clientX509Certificate.require", "true");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertTrue(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());
		
		config.log();
	}


	@Test
	public void testParse_withRequireClientCertificate_rootDNSpecified()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.clientX509Certificate.require", "true");
		props.setProperty("op.ssv.clientX509Certificate.rootDN", "CN=publisher.example.com");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertTrue(config.clientX509Certificate_require);
		assertEquals("CN=publisher.example.com", config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());
		
		config.log();
	}


	@Test
	public void testParse_requestTypeJWT_jwkSetSource_claimDriven()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.requestType", "JWT");
		props.setProperty("op.ssv.requestJWT.jwkSetSource", "urn:software_statement:org_jwks_endpoint");
		props.setProperty("op.ssv.requestJWT.jwsAlgorithms", "RS256,PS256");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JWT, config.requestType);
		assertEquals("urn:software_statement:org_jwks_endpoint", config.requestJWT_jwkSetSource.toString());
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.requestJWT_jwsAlgorithms);
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());
		
		config.log();
	}


	@Test
	public void testParse_requestTypeJWT_jwkSetSource_claimDriven_withRequiredJWTClaims()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.requestType", "JWT");
		props.setProperty("op.ssv.requestJWT.jwkSetSource", "urn:software_statement:org_jwks_endpoint");
		props.setProperty("op.ssv.requestJWT.jwsAlgorithms", "RS256,PS256");
		props.setProperty("op.ssv.requestJWT.requiredClaims", "iss,iat,exp,aud,jti");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JWT, config.requestType);
		assertEquals("urn:software_statement:org_jwks_endpoint", config.requestJWT_jwkSetSource.toString());
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.requestJWT_jwsAlgorithms);
		assertEquals(Set.of("iss", "iat", "exp", "aud", "jti"), config.requestJWT_requiredClaims);
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());
		
		config.log();
	}


	@Test
	public void testParse_requestTypeJWT_jwkSetSource_static()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.requestType", "JWT");
		props.setProperty("op.ssv.requestJWT.jwkSetSource", "https://client.example.com/jwks.json");
		props.setProperty("op.ssv.requestJWT.jwsAlgorithms", "RS256,PS256");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JWT, config.requestType);
		assertEquals("https://client.example.com/jwks.json", config.requestJWT_jwkSetSource.toString());
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.requestJWT_jwsAlgorithms);
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		assertTrue(config.scopeRules.isEmpty());
		
		config.log();
	}
	
	
	@Test
	public void testParse_transforms()
		throws IOException {
		
		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.transforms.rename.software_jwks_endpoint", "jwks_uri");
		props.setProperty("op.ssv.transforms.rename.software_client_name", "client_name");
		props.setProperty("op.ssv.transforms.moveIntoData", "org_id,org_contacts");
		props.setProperty("op.ssv.transforms.remove", "iss,iat,jti");
		
		var config = new Configuration(props);
		
		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		
		assertEquals(List.of("iss", "iat", "jti"), config.transforms_remove);
		
		assertEquals(
			Map.of(
				"software_jwks_endpoint", "jwks_uri",
				"software_client_name", "client_name"),
			config.transforms_rename);
		
		assertEquals(List.of("org_id", "org_contacts"), config.transforms_moveIntoData);
		
		assertTrue(config.scopeRules.isEmpty());
		
		config.log();
	}


	@Test
	public void testParse_scopeRules()
		throws IOException {
		
		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.setProperty("op.ssv.scopeRules.1.scope", "openid accounts");
		props.setProperty("op.ssv.scopeRules.1.jsonPath", "$.software_roles[?(@=='AISP')]");
		props.setProperty("op.ssv.scopeRules.2.scope", "openid payments");
		props.setProperty("op.ssv.scopeRules.2.jsonPath", "$.software_roles[?(@=='PISP')]");
		
		var config = new Configuration(props);
		
		assertTrue(config.enable);
		assertEquals(new Issuer("https://publisher.example.com"), config.issuer);
		assertEquals(new URL("https://publisher.example.com/jwks.json"), config.issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256), config.jwsAlgorithms);
		assertNull(config.jwtTypes);
		assertEquals(250, config.httpConnectTimeout);
		assertEquals(250, config.httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.registrationAccessToken.getValue());
		assertNull(config.registrationAccessToken.getScope());
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertEquals(Set.of("iss"), config.logClaims);
		assertFalse(config.clientX509Certificate_require);
		assertNull(config.clientX509Certificate_rootDN);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		
		assertEquals(new Scope("openid", "accounts"), config.scopeRules.get("1").getScope());
		assertEquals("$.software_roles[?(@=='AISP')]", config.scopeRules.get("1").getJSONPath());
		
		assertEquals(new Scope("openid", "payments"), config.scopeRules.get("2").getScope());
		assertEquals("$.software_roles[?(@=='PISP')]", config.scopeRules.get("2").getJSONPath());
		
		assertEquals(2, config.scopeRules.size());
		
		config.log();
	}


	@Test
	public void testParse_missingIssuer()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.remove("op.ssv.issuer");

		try {
			new Configuration(props);
			fail();
		}catch (ConfigurationException e) {
			assertEquals("Missing property: Property: op.ssv.issuer", e.getMessage());
		}
	}


	@Test
	public void testParse_missingIssuerJWKSetURL()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.remove("op.ssv.issuerJWKSetURL");

		try {
			new Configuration(props);
			fail();
		}catch (ConfigurationException e) {
			assertEquals("Missing property: Property: op.ssv.issuerJWKSetURL", e.getMessage());
		}
	}


	@Test
	public void testParse_missingJWSAlgorithms()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.remove("op.ssv.jwsAlgorithms");

		try {
			new Configuration(props);
			fail();
		}catch (ConfigurationException e) {
			assertEquals("Missing property: Property: op.ssv.jwsAlgorithms", e.getMessage());
		}
	}


	@Test
	public void testParse_missingRegistrationAccessToken()
		throws IOException {

		var props = new Properties();
		props.load(new FileReader("test.properties"));
		props.remove("op.ssv.registrationAccessToken");

		try {
			new Configuration(props);
			fail();
		}catch (ConfigurationException e) {
			assertEquals("Missing property: Property: op.ssv.registrationAccessToken", e.getMessage());
		}
	}
	
	
	@Test
	public void testDisabledByDefault() {
		
		var config = new Configuration(new Properties());
		assertFalse(config.enable);
		assertNull(config.issuer);
		assertNull(config.issuerJWKSetURL);
		assertTrue(config.jwsAlgorithms.isEmpty());
		assertNull(config.jwtTypes);
		assertEquals(0, config.httpConnectTimeout);
		assertEquals(0, config.httpReadTimeout);
		assertNull(config.registrationAccessToken);
		assertTrue(config.additionalRequiredClaims.isEmpty());
		assertTrue(config.logClaims.isEmpty());
		assertFalse(config.clientX509Certificate_require);
		assertEquals(RequestType.JSON, config.requestType);
		assertNull(config.requestJWT_jwkSetSource);
		assertTrue(config.requestJWT_jwsAlgorithms.isEmpty());
		assertTrue(config.requestJWT_requiredClaims.isEmpty());
		assertTrue(config.transforms_remove.isEmpty());
		assertTrue(config.transforms_rename.isEmpty());
		assertTrue(config.transforms_moveIntoData.isEmpty());
		
		config.log();
	}
}
