package com.nimbusds.openid.connect.provider.spi.reg.statement;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import net.minidev.json.JSONObject;
import org.junit.Test;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


public class JSONObjectTransformsTest {


	@Test
	public void testRename_memberNotPresent() {
		
		JSONObject jsonObject = new JSONObject();
		
		JSONObjectTransforms.rename(jsonObject, "m1", "m2");
		assertTrue(jsonObject.isEmpty());
	}

	@Test
	public void testRename_memberPresent() {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("m1", "value");
		
		JSONObjectTransforms.rename(jsonObject, "m1", "m2");
		assertEquals("value", jsonObject.get("m2"));
		assertEquals(1, jsonObject.size());
	}
	
	
	@Test
	public void testMoveIntoData_memberNotPresent() {
		
		JSONObject jsonObject = new JSONObject();
		
		JSONObjectTransforms.moveIntoData(jsonObject, "m");
		assertTrue(jsonObject.isEmpty());
	}

	@Test
	public void testMoveIntoData_memberPresent_dataNotPresent() throws ParseException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("m", "value");
		
		JSONObjectTransforms.moveIntoData(jsonObject, "m");
		
		JSONObject data = JSONObjectUtils.getJSONObject(jsonObject, "data");
		assertEquals("value", data.get("m"));
		assertEquals(1, data.size());
		
		assertEquals(1, jsonObject.size());
	}

	@Test
	public void testMoveIntoData_memberPresent_emptyDataPresent() throws ParseException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("m", "value");
		
		JSONObject data = new JSONObject();
		jsonObject.put("data", data);
		
		JSONObjectTransforms.moveIntoData(jsonObject, "m");
		
		data = JSONObjectUtils.getJSONObject(jsonObject, "data");
		assertEquals("value", data.get("m"));
		assertEquals(1, data.size());
		
		assertEquals(1, jsonObject.size());
	}

	@Test
	public void testMoveIntoData_memberPresent_overwriteDataOfOtherType() throws ParseException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("m", "value");
		jsonObject.put("data", "123");
		
		JSONObjectTransforms.moveIntoData(jsonObject, "m");
		
		JSONObject data = JSONObjectUtils.getJSONObject(jsonObject, "data");
		assertEquals("value", data.get("m"));
		assertEquals(1, data.size());
		
		assertEquals(1, jsonObject.size());
	}

	@Test
	public void testMoveIntoData_memberPresent_someDataPresent() throws ParseException {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("m", "value");
		
		JSONObject data = new JSONObject();
		data.put("n", "123");
		data.put("o", "456");
		jsonObject.put("data", data);
		
		JSONObjectTransforms.moveIntoData(jsonObject, "m");
		
		data = JSONObjectUtils.getJSONObject(jsonObject, "data");
		assertEquals("value", data.get("m"));
		assertEquals("123", data.get("n"));
		assertEquals("456", data.get("o"));
		assertEquals(3, data.size());
		
		assertEquals(1, jsonObject.size());
	}
	
	
	@Test
	public void testRemove_memberNotPresent() {
		
		JSONObject jsonObject = new JSONObject();
		
		JSONObjectTransforms.remove(jsonObject, "m");
		assertTrue(jsonObject.isEmpty());
	}
	
	@Test
	public void testRemove_memberPresent() {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("m", "value");
		
		JSONObjectTransforms.remove(jsonObject, "m");
		assertTrue(jsonObject.isEmpty());
	}
}
