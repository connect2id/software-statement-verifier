package com.nimbusds.openid.connect.provider.spi.reg.statement;


import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.*;

import org.junit.Test;


public class JWKSetSourceTest {

	
	@Test
	public void testConstructor_URN() throws URISyntaxException {
		
		var urn = new URI("urn:software_statement:org_jwks_endpoint");
		
		var jwkSetSource = new JWKSetSource(urn);
		
		assertNull(jwkSetSource.getStaticURL());
		assertEquals("org_jwks_endpoint", jwkSetSource.getURLClaimName());
		
		assertEquals(urn.toString(), jwkSetSource.toString());
	}

	
	@Test
	public void testConstructor_URL_https() throws URISyntaxException {
		
		var url = new URI("https://publisher.example.com/jwks.json");
		
		var jwkSetSource = new JWKSetSource(url);
		
		assertEquals(url.toString(), jwkSetSource.getStaticURL().toString());
		assertNull(jwkSetSource.getURLClaimName());
		
		assertEquals(url.toString(), jwkSetSource.toString());
	}

	
	@Test
	public void testConstructor_URL_http() throws URISyntaxException {
		
		var url = new URI("http://publisher.example.com/jwks.json");
		
		var jwkSetSource = new JWKSetSource(url);
		
		assertEquals(url.toString(), jwkSetSource.getStaticURL().toString());
		assertNull(jwkSetSource.getURLClaimName());
		
		assertEquals(url.toString(), jwkSetSource.toString());
	}

	
	@Test
	public void testConstructor_URL_relative() throws URISyntaxException {
		
		var url = new URI("/jwks.json");
		
		try {
			new JWKSetSource(url);
			fail();
		} catch (URISyntaxException e) {
			assertEquals("Missing URI scheme: /jwks.json", e.getMessage());
		}
	}
}
