package com.nimbusds.openid.connect.provider.spi.reg.statement;


import com.nimbusds.common.contenttype.ContentType;
import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.ECDSASigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.jwk.gen.ECKeyGenerator;
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import com.nimbusds.jwt.util.DateUtils;
import com.nimbusds.oauth2.sdk.ErrorObject;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.client.RegistrationError;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.JWTID;
import com.nimbusds.oauth2.sdk.id.SoftwareID;
import com.nimbusds.oauth2.sdk.util.X509CertificateUtils;
import com.nimbusds.openid.connect.provider.spi.reg.InterceptorContext;
import com.nimbusds.openid.connect.provider.spi.reg.WrappedHTTPResponseException;
import com.nimbusds.openid.connect.sdk.rp.ApplicationType;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;
import com.thetransactioncompany.util.PropertyFilter;
import net.minidev.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static net.jadler.Jadler.*;
import static org.junit.Assert.*;


public class SoftwareStatementVerifierTest {
	
	
	static final Issuer OP_ISSUER = new Issuer("https://c2id.com");
	
	
	static final Issuer PUBLISHER_ISSUER = new Issuer("https://publisher.example.com");
	
	
	static final RSAKey PUBLISHER_JWK_RS256;
	
	
	static final RSAKey PUBLISHER_JWK_PS256;
	
	
	static final ECKey PUBLISHER_JWK_ES256;
	
	
	static final JWKSet PUBLISHER_JWK_SET;
	
	
	static final Issuer CLIENT_ISSUER = new Issuer("https://client.example.com");
	
	
	static final RSAKey CLIENT_JWK_RS256;
	
	
	static final JWKSet CLIENT_JWK_SET;
	
	
	enum RequestJWKSetSourceType {
		STATIC, DYNAMIC
	}
	
	
	static final InterceptorContext INTERCEPTOR_CTX = new InterceptorContext() {
		@Override
		public boolean openRegistrationIsAllowed() {
			return false;
		}
		
		
		@Override
		public Issuer getIssuer() {
			return OP_ISSUER;
		}
	};
	
	
	static {
		try {
			PUBLISHER_JWK_RS256 = new RSAKeyGenerator(2048)
				.keyUse(KeyUse.SIGNATURE)
				.algorithm(JWSAlgorithm.RS256)
				.keyID("1")
				.generate();
			
			PUBLISHER_JWK_PS256 = new RSAKeyGenerator(2048)
				.keyUse(KeyUse.SIGNATURE)
				.algorithm(JWSAlgorithm.PS256)
				.keyID("3")
				.generate();
			
			PUBLISHER_JWK_ES256 = new ECKeyGenerator(Curve.P_256)
				.keyUse(KeyUse.SIGNATURE)
				.algorithm(JWSAlgorithm.ES256)
				.keyID("3")
				.generate();
			
			PUBLISHER_JWK_SET = new JWKSet(List.of(
				PUBLISHER_JWK_RS256,
				PUBLISHER_JWK_PS256,
				PUBLISHER_JWK_ES256));
			
			
			CLIENT_JWK_RS256 = new RSAKeyGenerator(2048)
				.keyUse(KeyUse.SIGNATURE)
				.algorithm(JWSAlgorithm.RS256)
				.keyID("S1")
				.generate();
			
			CLIENT_JWK_SET = new JWKSet(CLIENT_JWK_RS256);
			
		} catch (JOSEException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Before
	public void setUp() {
		
		initJadler();
		
		// Set up publisher JWK set endpoint
		onRequest()
			.havingMethodEqualTo("GET")
			.havingPathEqualTo("/publisher/jwks.json")
			.respond()
			.withStatus(200)
			.withBody(PUBLISHER_JWK_SET.toPublicJWKSet().toString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");
		
		// Set up client JWK set endpoint
		onRequest()
			.havingMethodEqualTo("GET")
			.havingPathEqualTo("/client/jwks.json")
			.respond()
			.withStatus(200)
			.withBody(CLIENT_JWK_SET.toPublicJWKSet().toString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");
	}


	@After
	public void tearDown() {
		closeJadler();
		
		var propsToClear = PropertyFilter.filterWithPrefix("op.ssv.", System.getProperties());
		
		for (var propName: propsToClear.stringPropertyNames()) {
			System.clearProperty(propName);
		}
	}
	
	
	private SoftwareStatementVerifier createConfiguredVerifier()
		throws Exception {
		
		var verifier = new SoftwareStatementVerifier();
		verifier.init(new MockServletInitContext());
		
		System.out.println("Test publisher JWK set URL: " + verifier.getConfiguration().issuerJWKSetURL);
		
		return verifier;
	}
	
	
	@Test
	public void testConfiguration()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		
		assertTrue(verifier.getConfiguration().enable);
		assertEquals(PUBLISHER_ISSUER, verifier.getConfiguration().issuer);
		assertEquals(new URL("http://localhost:" + port() + "/publisher/jwks.json"), verifier.getConfiguration().issuerJWKSetURL);
		assertEquals(Set.of(JWSAlgorithm.RS256, JWSAlgorithm.PS256, JWSAlgorithm.ES256), verifier.getConfiguration().jwsAlgorithms);
		assertEquals(250, verifier.getConfiguration().httpConnectTimeout);
		assertEquals(250, verifier.getConfiguration().httpReadTimeout);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", verifier.getConfiguration().registrationAccessToken.getValue());
	}
	
	
	@Test
	public void testDisabled()
		throws Exception {
		
		System.setProperty("op.ssv.enable", "false");
		System.setProperty("op.ssv.issuer", "");
		System.setProperty("op.ssv.issuerJWKSetURL", "");
		
		var verifier = createConfiguredVerifier();
		
		assertFalse(verifier.isEnabled());
		
		assertFalse(verifier.getConfiguration().enable);
		assertNull(verifier.getConfiguration().issuer);
		assertNull(verifier.getConfiguration().issuerJWKSetURL);
		assertTrue(verifier.getConfiguration().jwsAlgorithms.isEmpty());
		assertEquals(0, verifier.getConfiguration().httpConnectTimeout);
		assertEquals(0, verifier.getConfiguration().httpReadTimeout);
		assertNull(verifier.getConfiguration().registrationAccessToken);
		
		// pass through
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		
		var out = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		assertEquals(httpRequest.getMethod(), out.getMethod());
		assertEquals(httpRequest.getEntityContentType(), out.getEntityContentType());
		assertNull(out.getAuthorization());
		assertEquals(httpRequest.getBody(), out.getBody());
	}
	
	
	@Test
	public void testNoSoftwareStatement_passThrough()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		
		// scope and custom params pass
		clientMetadata.setScope(new Scope("openid", "email"));
		clientMetadata.setCustomField("data", "xyz");
		clientMetadata.setCustomField("preferred_client_id", "123");
		clientMetadata.setCustomField("preferred_client_secret", "bohcaig9naiP0imohqueew6iegh8ohno");
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		var out = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		assertEquals(httpRequest.getMethod(), out.getMethod());
		assertEquals(httpRequest.getEntityContentType(), out.getEntityContentType());
		assertNull(out.getAuthorization());
		assertEquals(httpRequest.getBody(), out.getBody());
	}
	
	
	@Test
	public void testWithSoftwareStatement()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		
		
		for (JWK jwk: PUBLISHER_JWK_SET.getKeys()) {
			
			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) jwk.getAlgorithm())
					.keyID(jwk.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());
			
			JWSSigner signer;
			if (JWSAlgorithm.Family.RSA.contains(jwk.getAlgorithm())) {
				signer = new RSASSASigner(jwk.toRSAKey());
			} else if (JWSAlgorithm.Family.EC.contains(jwk.getAlgorithm())) {
				signer = new ECDSASigner(jwk.toECKey());
			} else {
				throw new RuntimeException();
			}
			
			jwt.sign(signer);
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);
			
			// top-level scope and custom params must be scrubbed
			clientMetadata.setScope(new Scope("openid", "email"));
			clientMetadata.setCustomField("data", "xyz");
			clientMetadata.setCustomField("preferred_client_id", "123");
			clientMetadata.setCustomField("preferred_client_secret", "bohcaig9naiP0imohqueew6iegh8ohno");
			
			var verifier = createConfiguredVerifier();
			
			assertTrue(verifier.isEnabled());
			
			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
			httpRequest.setBody(clientMetadata.toString());
			
			var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			
			assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
			assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
			assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
			
			var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
			assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
			assertEquals(statementMetadata.getName(), mergedMetadata.getName());
			assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
			assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
			assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
			assertNull(mergedMetadata.getSoftwareStatement());
			assertNull("scrubbed", mergedMetadata.getScope());
			assertTrue("scrubbed", mergedMetadata.getCustomFields().isEmpty());
		}
	}


	@Test
	public void testWithSoftwareStatement_explicitJWTType()
		throws Exception {

		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.jwtTypes", "software-statement+jwt");

		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));

		var jwtType = new JOSEObjectType("software-statement+jwt");

		for (JWK jwk: PUBLISHER_JWK_SET.getKeys()) {

			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) jwk.getAlgorithm())
					.type(jwtType)
					.keyID(jwk.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());

			JWSSigner signer;
			if (JWSAlgorithm.Family.RSA.contains(jwk.getAlgorithm())) {
				signer = new RSASSASigner(jwk.toRSAKey());
			} else if (JWSAlgorithm.Family.EC.contains(jwk.getAlgorithm())) {
				signer = new ECDSASigner(jwk.toECKey());
			} else {
				throw new RuntimeException();
			}

			jwt.sign(signer);

			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);

			// top-level scope and custom params must be scrubbed
			clientMetadata.setScope(new Scope("openid", "email"));
			clientMetadata.setCustomField("data", "xyz");
			clientMetadata.setCustomField("preferred_client_id", "123");
			clientMetadata.setCustomField("preferred_client_secret", "bohcaig9naiP0imohqueew6iegh8ohno");

			var verifier = createConfiguredVerifier();

			assertTrue(verifier.isEnabled());

			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
			httpRequest.setBody(clientMetadata.toString());

			var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);

			assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
			assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
			assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());

			var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
			assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
			assertEquals(statementMetadata.getName(), mergedMetadata.getName());
			assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
			assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
			assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
			assertNull(mergedMetadata.getSoftwareStatement());
			assertNull("scrubbed", mergedMetadata.getScope());
			assertTrue("scrubbed", mergedMetadata.getCustomFields().isEmpty());
		}
	}


	@Test
	public void testWithSoftwareStatement_explicitJWTType_notAccepted()
		throws Exception {

		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.jwtType", "software-statement+jwt");

		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));

		var jwtType = new JOSEObjectType("some-jwt-type");

		for (JWK jwk: PUBLISHER_JWK_SET.getKeys()) {

			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) jwk.getAlgorithm())
					.type(jwtType)
					.keyID(jwk.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());

			JWSSigner signer;
			if (JWSAlgorithm.Family.RSA.contains(jwk.getAlgorithm())) {
				signer = new RSASSASigner(jwk.toRSAKey());
			} else if (JWSAlgorithm.Family.EC.contains(jwk.getAlgorithm())) {
				signer = new ECDSASigner(jwk.toECKey());
			} else {
				throw new RuntimeException();
			}

			jwt.sign(signer);

			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);

			// top-level scope and custom params must be scrubbed
			clientMetadata.setScope(new Scope("openid", "email"));
			clientMetadata.setCustomField("data", "xyz");
			clientMetadata.setCustomField("preferred_client_id", "123");
			clientMetadata.setCustomField("preferred_client_secret", "bohcaig9naiP0imohqueew6iegh8ohno");

			var verifier = createConfiguredVerifier();

			assertTrue(verifier.isEnabled());

			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
			httpRequest.setBody(clientMetadata.toString());

			try {
				verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
				fail();
			} catch (WrappedHTTPResponseException e) {
				assertEquals("Invalid software statement JWT: JOSE header typ (type) some-jwt-type not allowed", e.getMessage());
			}
		}
	}


	@Test
	public void testWithSoftwareStatement_explicitJWTType_noTypHeader()
		throws Exception {

		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.jwtTypes", "software-statement+jwt");

		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));

		for (JWK jwk: PUBLISHER_JWK_SET.getKeys()) {

			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) jwk.getAlgorithm())
					.keyID(jwk.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());

			JWSSigner signer;
			if (JWSAlgorithm.Family.RSA.contains(jwk.getAlgorithm())) {
				signer = new RSASSASigner(jwk.toRSAKey());
			} else if (JWSAlgorithm.Family.EC.contains(jwk.getAlgorithm())) {
				signer = new ECDSASigner(jwk.toECKey());
			} else {
				throw new RuntimeException();
			}

			jwt.sign(signer);

			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);

			// top-level scope and custom params must be scrubbed
			clientMetadata.setScope(new Scope("openid", "email"));
			clientMetadata.setCustomField("data", "xyz");
			clientMetadata.setCustomField("preferred_client_id", "123");
			clientMetadata.setCustomField("preferred_client_secret", "bohcaig9naiP0imohqueew6iegh8ohno");

			var verifier = createConfiguredVerifier();

			assertTrue(verifier.isEnabled());

			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
			httpRequest.setBody(clientMetadata.toString());

			try {
				verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
				fail();
			} catch (WrappedHTTPResponseException e) {
				assertEquals("Invalid software statement JWT: Required JOSE header typ (type) parameter is missing", e.getMessage());
			}
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_scopeRules()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.scopeRules.1.scope", "openid accounts");
		System.setProperty("op.ssv.scopeRules.1.jsonPath", "$.software_roles[?(@=='AISP')]");
		System.setProperty("op.ssv.scopeRules.2.scope", "openid payments");
		System.setProperty("op.ssv.scopeRules.2.jsonPath", "$.software_roles[?(@=='PISP')]");
		System.setProperty("op.ssv.transforms.remove", "software_roles");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		statementMetadata.setCustomField("software_roles", List.of("AISP", "PISP", "CBPII"));
		
		for (JWK jwk: PUBLISHER_JWK_SET.getKeys()) {
			
			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) jwk.getAlgorithm())
					.keyID(jwk.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());
			
			JWSSigner signer;
			if (JWSAlgorithm.Family.RSA.contains(jwk.getAlgorithm())) {
				signer = new RSASSASigner(jwk.toRSAKey());
			} else if (JWSAlgorithm.Family.EC.contains(jwk.getAlgorithm())) {
				signer = new ECDSASigner(jwk.toECKey());
			} else {
				throw new RuntimeException();
			}
			
			jwt.sign(signer);
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);
			
			// top-level scope matches rules and must be allowed
			clientMetadata.setScope(new Scope("openid", "accounts", "payments"));
			
			var verifier = createConfiguredVerifier();
			
			assertTrue(verifier.isEnabled());
			
			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
			httpRequest.setBody(clientMetadata.toString());
			
			var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			
			assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
			assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
			assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
			
			var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
			
			assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
			assertEquals(statementMetadata.getName(), mergedMetadata.getName());
			assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
			assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
			assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
			assertNull(mergedMetadata.getSoftwareStatement());
			assertEquals(new Scope("openid", "accounts", "payments"), mergedMetadata.getScope());
			assertTrue("scrubbed", mergedMetadata.getCustomFields().isEmpty());
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_scopeRules_withSoftwareStatementScope()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.scopeRules.1.scope", "openid accounts");
		System.setProperty("op.ssv.scopeRules.1.jsonPath", "$.software_roles[?(@=='AISP')]");
		System.setProperty("op.ssv.scopeRules.2.scope", "openid payments");
		System.setProperty("op.ssv.scopeRules.2.jsonPath", "$.software_roles[?(@=='PISP')]");
		System.setProperty("op.ssv.transforms.remove", "software_roles");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		statementMetadata.setScope(new Scope("directory"));
		statementMetadata.setCustomField("software_roles", List.of("AISP", "PISP", "CBPII"));
		
		for (JWK jwk: PUBLISHER_JWK_SET.getKeys()) {
			
			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) jwk.getAlgorithm())
					.keyID(jwk.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());
			
			JWSSigner signer;
			if (JWSAlgorithm.Family.RSA.contains(jwk.getAlgorithm())) {
				signer = new RSASSASigner(jwk.toRSAKey());
			} else if (JWSAlgorithm.Family.EC.contains(jwk.getAlgorithm())) {
				signer = new ECDSASigner(jwk.toECKey());
			} else {
				throw new RuntimeException();
			}
			
			jwt.sign(signer);
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);
			
			// top-level scope matches rules and must be allowed
			clientMetadata.setScope(new Scope("openid", "accounts", "payments"));
			
			var verifier = createConfiguredVerifier();
			
			assertTrue(verifier.isEnabled());
			
			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
			httpRequest.setBody(clientMetadata.toString());
			
			var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			
			assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
			assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
			assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
			
			var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
			
			assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
			assertEquals(statementMetadata.getName(), mergedMetadata.getName());
			assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
			assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
			assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
			assertNull(mergedMetadata.getSoftwareStatement());
			assertEquals(new Scope("openid", "accounts", "payments", "directory"), mergedMetadata.getScope());
			assertTrue("scrubbed", mergedMetadata.getCustomFields().isEmpty());
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_jsonObjectTransforms()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.transforms.remove", "iss,iat,jti");
		System.setProperty("op.ssv.transforms.rename.software_jwks_endpoint", "jwks_uri");
		System.setProperty("op.ssv.transforms.rename.software_client_name", "client_name");
		System.setProperty("op.ssv.transforms.moveIntoData", "org_id,org_contacts");
		
		var jwksURI = URI.create("https://client.example.com/jwks.json");
		var clientName = "Example client";
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		
		// to be renamed
		statementMetadata.setCustomField("software_jwks_endpoint", jwksURI.toString());
		statementMetadata.setCustomField("software_client_name", clientName);
		
		// to be moved into "data"
		statementMetadata.setCustomField("org_id", "Some Client Organization");
		statementMetadata.setCustomField("org_contacts", List.of("admin@client.example.com", "webmaster@client.example.com"));
		
		var jwt = new SignedJWT(
			new JWSHeader.Builder((JWSAlgorithm) PUBLISHER_JWK_PS256.getAlgorithm())
				.keyID(PUBLISHER_JWK_PS256.getKeyID())
				.build(),
			new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
				.issuer(PUBLISHER_ISSUER.getValue()) // to be removed
				.issueTime(new Date()) // to be removed
				.jwtID(new JWTID().getValue()) // to be removed
				.build());
		
		JWSSigner signer = new RSASSASigner(PUBLISHER_JWK_PS256.toRSAKey());
		
		jwt.sign(signer);
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		clientMetadata.setSoftwareStatement(jwt);
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		
		assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
		assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
		assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
		
		var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
		
		System.out.println(mergedMetadata);
		
		assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
		assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
		
		// renamed
		assertEquals(jwksURI, mergedMetadata.getJWKSetURI());
		assertEquals(clientName, mergedMetadata.getName());
		
		// moved into data
		JSONObject data = (JSONObject) mergedMetadata.getCustomField("data");
		assertEquals(statementMetadata.getCustomField("org_id"), data.get("org_id"));
		assertEquals(statementMetadata.getCustomField("org_contacts"), data.get("org_contacts"));
		assertEquals(2, data.size());
		
		assertNull(mergedMetadata.getSoftwareStatement());
		
		assertEquals(5, mergedMetadata.toJSONObject().size());
	}
	
	
	@Test
	public void testWithSoftwareStatement_requestJWT()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.requestType", "JWT");
		System.setProperty("op.ssv.requestJWT.jwsAlgorithms", "RS256");
		
		for (var srcType: RequestJWKSetSourceType.values()) {
			
			System.out.println("Request JWK set source: " + srcType);
			
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "urn:software_statement:org_jwks_endpoint");
			} else if (srcType.equals(RequestJWKSetSourceType.STATIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "http://localhost:" + port() + "/client/jwks.json");
			}
			
			var statementMetadata = new OIDCClientMetadata();
			statementMetadata.setApplicationType(ApplicationType.WEB);
			statementMetadata.setName("Example client");
			statementMetadata.setURI(URI.create("https://client.example.com"));
			statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				statementMetadata.setCustomField("org_jwks_endpoint", "http://localhost:" + port() + "/client/jwks.json");
			}
			
			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) PUBLISHER_JWK_PS256.getAlgorithm())
					.keyID(PUBLISHER_JWK_PS256.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());
			
			JWSSigner signer = new RSASSASigner(PUBLISHER_JWK_PS256.toRSAKey());
			
			jwt.sign(signer);
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);
			
			// top-level scope and custom params must be scrubbed
			clientMetadata.setScope(new Scope("openid", "email"));
			clientMetadata.setCustomField("data", "xyz");
			clientMetadata.setCustomField("preferred_client_id", "123");
			clientMetadata.setCustomField("preferred_client_secret", "bohcaig9naiP0imohqueew6iegh8ohno");
			
			var requestJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(CLIENT_JWK_RS256.getKeyID()).build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(clientMetadata.toJSONObject()))
					.issuer(CLIENT_ISSUER.getValue())
					.build());
			requestJWT.sign(new RSASSASigner(CLIENT_JWK_RS256));
			
			var verifier = createConfiguredVerifier();
			
			assertTrue(verifier.isEnabled());
			
			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JWT.toString());
			httpRequest.setBody(requestJWT.serialize());
			
			var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			
			assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
			assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
			assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
			
			var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
			assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
			assertEquals(statementMetadata.getName(), mergedMetadata.getName());
			assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
			assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
			assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
			assertNull(mergedMetadata.getSoftwareStatement());
			
			assertNull("scrubbed", mergedMetadata.getScope());
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				assertEquals("http://localhost:" + port() + "/client/jwks.json", mergedMetadata.getCustomField("org_jwks_endpoint"));
				assertEquals(1, mergedMetadata.getCustomFields().size());
			} else if (srcType.equals(RequestJWKSetSourceType.STATIC)) {
				assertTrue("scrubbed", mergedMetadata.getCustomFields().isEmpty());
			}
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_requestJWT_withRequiredJWTClaims()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.requestType", "JWT");
		System.setProperty("op.ssv.requestJWT.jwsAlgorithms", "RS256");
		System.setProperty("op.ssv.requestJWT.requiredClaims", "aud,jti,exp");
		
		for (var srcType: RequestJWKSetSourceType.values()) {
			
			System.out.println("Request JWK set source: " + srcType);
			
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "urn:software_statement:org_jwks_endpoint");
			} else if (srcType.equals(RequestJWKSetSourceType.STATIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "http://localhost:" + port() + "/client/jwks.json");
			}
			
			var statementMetadata = new OIDCClientMetadata();
			statementMetadata.setApplicationType(ApplicationType.WEB);
			statementMetadata.setName("Example client");
			statementMetadata.setURI(URI.create("https://client.example.com"));
			statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				statementMetadata.setCustomField("org_jwks_endpoint", "http://localhost:" + port() + "/client/jwks.json");
			}
			
			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) PUBLISHER_JWK_PS256.getAlgorithm())
					.keyID(PUBLISHER_JWK_PS256.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());
			
			JWSSigner signer = new RSASSASigner(PUBLISHER_JWK_PS256.toRSAKey());
			
			jwt.sign(signer);
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);
			
			var requestJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(CLIENT_JWK_RS256.getKeyID()).build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(clientMetadata.toJSONObject()))
					.issuer(CLIENT_ISSUER.getValue())
					.audience(OP_ISSUER.getValue())
					.expirationTime(Date.from(Instant.now().plus(10, ChronoUnit.MINUTES)))
					.jwtID(new JWTID().getValue())
					.build());
			requestJWT.sign(new RSASSASigner(CLIENT_JWK_RS256));
			
			var verifier = createConfiguredVerifier();
			
			assertTrue(verifier.isEnabled());
			
			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JWT.toString());
			httpRequest.setBody(requestJWT.serialize());
			
			var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			
			assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
			assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
			assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
			
			var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
			assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
			assertEquals(statementMetadata.getName(), mergedMetadata.getName());
			assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
			assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
			assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
			assertNull(mergedMetadata.getSoftwareStatement());
			
			// Missing audience
			requestJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(CLIENT_JWK_RS256.getKeyID()).build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(clientMetadata.toJSONObject()))
					.issuer(CLIENT_ISSUER.getValue())
					.expirationTime(Date.from(Instant.now().plus(10, ChronoUnit.MINUTES)))
					.jwtID(new JWTID().getValue())
					.build());
			requestJWT.sign(new RSASSASigner(CLIENT_JWK_RS256));
			
			httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JWT.toString());
			httpRequest.setBody(requestJWT.serialize());
			
			try {
				verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
				fail();
			} catch (WrappedHTTPResponseException e) {
				var httpResponse = e.getHTTPResponse();
				assertEquals(400, httpResponse.getStatusCode());
				assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
				var errorObject = ErrorObject.parse(httpResponse);
				assertEquals(400, errorObject.getHTTPStatusCode());
				assertEquals(OAuth2Error.INVALID_REQUEST.getCode(), errorObject.getCode());
				assertEquals("Invalid registration JWT: Audience not accepted: Missing claim", errorObject.getDescription());
				assertNull(errorObject.getURI());
			}
			
			// Audience doesn't match expected
			requestJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(CLIENT_JWK_RS256.getKeyID()).build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(clientMetadata.toJSONObject()))
					.issuer(CLIENT_ISSUER.getValue())
					.audience("https://some-other-idp.com")
					.expirationTime(Date.from(Instant.now().plus(10, ChronoUnit.MINUTES)))
					.jwtID(new JWTID().getValue())
					.build());
			requestJWT.sign(new RSASSASigner(CLIENT_JWK_RS256));
			
			httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JWT.toString());
			httpRequest.setBody(requestJWT.serialize());
			
			try {
				verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
				fail();
			} catch (WrappedHTTPResponseException e) {
				var httpResponse = e.getHTTPResponse();
				assertEquals(400, httpResponse.getStatusCode());
				assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
				var errorObject = ErrorObject.parse(httpResponse);
				assertEquals(400, errorObject.getHTTPStatusCode());
				assertEquals(OAuth2Error.INVALID_REQUEST.getCode(), errorObject.getCode());
				assertEquals("Invalid registration JWT: Audience not accepted: https://some-other-idp.com", errorObject.getDescription());
				assertNull(errorObject.getURI());
			}
			
			// Missing exp claim
			requestJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(CLIENT_JWK_RS256.getKeyID()).build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(clientMetadata.toJSONObject()))
					.issuer(CLIENT_ISSUER.getValue())
					.audience(OP_ISSUER.getValue())
					.jwtID(new JWTID().getValue())
					.build());
			requestJWT.sign(new RSASSASigner(CLIENT_JWK_RS256));
			
			httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JWT.toString());
			httpRequest.setBody(requestJWT.serialize());
			
			try {
				verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
				fail();
			} catch (WrappedHTTPResponseException e) {
				var httpResponse = e.getHTTPResponse();
				assertEquals(400, httpResponse.getStatusCode());
				assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
				var errorObject = ErrorObject.parse(httpResponse);
				assertEquals(400, errorObject.getHTTPStatusCode());
				assertEquals(OAuth2Error.INVALID_REQUEST.getCode(), errorObject.getCode());
				assertEquals("Invalid registration JWT: JWT missing required claims: [exp]", errorObject.getDescription());
				assertNull(errorObject.getURI());
			}
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_requestJWT_byPassPlainRegistrationRequest()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.requestType", "JWT");
		System.setProperty("op.ssv.requestJWT.jwsAlgorithms", "RS256");
		
		for (var srcType: RequestJWKSetSourceType.values()) {
			
			System.out.println("Request JWK set source: " + srcType);
			
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "urn:software_statement:org_jwks_endpoint");
			} else if (srcType.equals(RequestJWKSetSourceType.STATIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "http://localhost:" + port() + "/client/jwks.json");
			}
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			
			var verifier = createConfiguredVerifier();
			
			assertTrue(verifier.isEnabled());
			
			// Content-Type: application/json
			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
			httpRequest.setBody(clientMetadata.toString());
			
			var outputHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			assertEquals(httpRequest, outputHTTPRequest);
			
			// Content-Type missing
			httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setBody(clientMetadata.toString());
			
			outputHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			assertEquals(httpRequest, outputHTTPRequest);
			
			// Empty entity body
			httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			
			outputHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			assertEquals(httpRequest, outputHTTPRequest);
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_requestJWT_clientJWKSetURL_404()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.requestType", "JWT");
		System.setProperty("op.ssv.requestJWT.jwsAlgorithms", "RS256");
		
		for (var srcType: RequestJWKSetSourceType.values()) {
			
			System.out.println("Request JWK set source: " + srcType);
			
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "urn:software_statement:org_jwks_endpoint");
			} else if (srcType.equals(RequestJWKSetSourceType.STATIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "http://localhost:" + port() + "/client/no-such-jwks.json"); // 404
			}
			
			var statementMetadata = new OIDCClientMetadata();
			statementMetadata.setApplicationType(ApplicationType.WEB);
			statementMetadata.setName("Example client");
			statementMetadata.setURI(URI.create("https://client.example.com"));
			statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				statementMetadata.setCustomField("org_jwks_endpoint", "http://localhost:" + port() + "/client/no-such-jwks.json"); // 404
			}
			
			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) PUBLISHER_JWK_PS256.getAlgorithm())
					.keyID(PUBLISHER_JWK_PS256.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());
			
			JWSSigner signer = new RSASSASigner(PUBLISHER_JWK_PS256.toRSAKey());
			
			jwt.sign(signer);
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);
			
			var requestJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(CLIENT_JWK_RS256.getKeyID()).build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(clientMetadata.toJSONObject()))
					.issuer(CLIENT_ISSUER.getValue())
					.build());
			requestJWT.sign(new RSASSASigner(CLIENT_JWK_RS256));
			
			var verifier = createConfiguredVerifier();
			
			assertTrue(verifier.isEnabled());
			
			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JWT.toString());
			httpRequest.setBody(requestJWT.serialize());
			
			try {
				verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
				fail();
			} catch (WrappedHTTPResponseException e) {
				var httpResponse = e.getHTTPResponse();
				assertEquals(400, httpResponse.getStatusCode());
				assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
				var errorObject = ErrorObject.parse(httpResponse);
				assertEquals(400, errorObject.getHTTPStatusCode());
				assertEquals(OAuth2Error.INVALID_REQUEST.getCode(), errorObject.getCode());
				assertEquals("Registration JWT validation failed: Couldn't retrieve remote JWK set: http://localhost:" + port() + "/client/no-such-jwks.json", errorObject.getDescription());
				assertNull(errorObject.getURI());
			}
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_requestJWT_invalidSignature()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.requestType", "JWT");
		System.setProperty("op.ssv.requestJWT.jwsAlgorithms", "RS256");
		
		for (var srcType: RequestJWKSetSourceType.values()) {
			
			System.out.println("Request JWK set source: " + srcType);
			
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "urn:software_statement:org_jwks_endpoint");
			} else if (srcType.equals(RequestJWKSetSourceType.STATIC)) {
				System.setProperty("op.ssv.requestJWT.jwkSetSource", "http://localhost:" + port() + "/client/jwks.json");
			}
			
			var statementMetadata = new OIDCClientMetadata();
			statementMetadata.setApplicationType(ApplicationType.WEB);
			statementMetadata.setName("Example client");
			statementMetadata.setURI(URI.create("https://client.example.com"));
			statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
			if (srcType.equals(RequestJWKSetSourceType.DYNAMIC)) {
				statementMetadata.setCustomField("org_jwks_endpoint", "http://localhost:" + port() + "/client/jwks.json");
			}
			
			var jwt = new SignedJWT(
				new JWSHeader.Builder((JWSAlgorithm) PUBLISHER_JWK_PS256.getAlgorithm())
					.keyID(PUBLISHER_JWK_PS256.getKeyID())
					.build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
					.issuer(PUBLISHER_ISSUER.getValue())
					.build());
			
			JWSSigner signer = new RSASSASigner(new RSAKeyGenerator(2048).generate().toRSAKey()); // unknown key
			
			jwt.sign(signer);
			
			var clientMetadata = new OIDCClientMetadata();
			clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
			clientMetadata.setSoftwareStatement(jwt);
			
			var requestJWT = new SignedJWT(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(CLIENT_JWK_RS256.getKeyID()).build(),
				new JWTClaimsSet.Builder(JWTClaimsSet.parse(clientMetadata.toJSONObject()))
					.issuer(CLIENT_ISSUER.getValue())
					.build());
			requestJWT.sign(new RSASSASigner(CLIENT_JWK_RS256));
			
			var verifier = createConfiguredVerifier();
			
			assertTrue(verifier.isEnabled());
			
			var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
			httpRequest.setContentType(ContentType.APPLICATION_JWT.toString());
			httpRequest.setBody(requestJWT.serialize());
			
			try {
				verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
				fail();
			} catch (WrappedHTTPResponseException e) {
				var httpResponse = e.getHTTPResponse();
				assertEquals(400, httpResponse.getStatusCode());
				assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
				var errorObject = ErrorObject.parse(httpResponse);
				assertEquals(400, errorObject.getHTTPStatusCode());
				assertEquals(RegistrationError.INVALID_SOFTWARE_STATEMENT.getCode(), errorObject.getCode());
				assertEquals("Invalid software statement JWT: Signed JWT rejected: Invalid signature", errorObject.getDescription());
				assertNull(errorObject.getURI());
			}
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_requireClientCertificate()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.clientX509Certificate.require", "true");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		
		
		var jwt = new SignedJWT(
			new JWSHeader.Builder((JWSAlgorithm) PUBLISHER_JWK_PS256.getAlgorithm())
				.keyID(PUBLISHER_JWK_PS256.getKeyID())
				.build(),
			new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
				.issuer(PUBLISHER_ISSUER.getValue())
				.build());
		
		JWSSigner signer = new RSASSASigner(PUBLISHER_JWK_PS256.toRSAKey());
		
		jwt.sign(signer);
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		clientMetadata.setSoftwareStatement(jwt);
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		assertTrue(verifier.getConfiguration().clientX509Certificate_require);
		
		// Client certificate missing
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		var outputHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		assertEquals(httpRequest, outputHTTPRequest);
		
		// Add client certificate
		var now = new Date();
		
		var clientCert = X509CertificateUtils.generateSelfSigned(
			PUBLISHER_ISSUER,
			now,
			DateUtils.fromSecondsSinceEpoch(DateUtils.toSecondsSinceEpoch(now) + 3600),
			PUBLISHER_JWK_RS256.toPublicKey(),
			PUBLISHER_JWK_RS256.toPrivateKey());
		
		httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setClientX509Certificate(clientCert);
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		
		assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
		assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
		assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
		
		var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
		assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
		assertEquals(statementMetadata.getName(), mergedMetadata.getName());
		assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
		assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
		assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
		assertNull(mergedMetadata.getSoftwareStatement());
	}
	
	
	@Test
	public void testWithSoftwareStatement_requireClientCertificate_withSpecifiedRootDN()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.clientX509Certificate.require", "true");
		System.setProperty("op.ssv.clientX509Certificate.rootDN", "CN=" + PUBLISHER_ISSUER.getValue());
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		
		
		var jwt = new SignedJWT(
			new JWSHeader.Builder((JWSAlgorithm) PUBLISHER_JWK_PS256.getAlgorithm())
				.keyID(PUBLISHER_JWK_PS256.getKeyID())
				.build(),
			new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
				.issuer(PUBLISHER_ISSUER.getValue())
				.build());
		
		JWSSigner signer = new RSASSASigner(PUBLISHER_JWK_PS256.toRSAKey());
		
		jwt.sign(signer);
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		clientMetadata.setSoftwareStatement(jwt);
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		assertTrue(verifier.getConfiguration().clientX509Certificate_require);
		assertEquals("CN=" + PUBLISHER_ISSUER.getValue(), verifier.getConfiguration().clientX509Certificate_rootDN);
		
		// Client certificate missing
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setClientX509CertificateRootDN(PUBLISHER_ISSUER.getValue());
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		var outputHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		assertEquals(httpRequest, outputHTTPRequest);
		
		// Add valid client certificate
		var now = new Date();
		
		var clientCert = X509CertificateUtils.generateSelfSigned(
			PUBLISHER_ISSUER,
			now,
			DateUtils.fromSecondsSinceEpoch(DateUtils.toSecondsSinceEpoch(now) + 3600),
			PUBLISHER_JWK_RS256.toPublicKey(),
			PUBLISHER_JWK_RS256.toPrivateKey());
		
		httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setClientX509Certificate(clientCert);
		httpRequest.setClientX509CertificateRootDN(clientCert.getIssuerDN().getName());
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		
		assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
		assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
		assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
		
		var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
		assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
		assertEquals(statementMetadata.getName(), mergedMetadata.getName());
		assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
		assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
		assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
		assertNull(mergedMetadata.getSoftwareStatement());
		
		// Client certificate has other root DN
		var otherJWK = new RSAKeyGenerator(2048).generate();
		clientCert = X509CertificateUtils.generateSelfSigned(
			new Issuer("https://some-other-publisher.example.com"),
			now,
			DateUtils.fromSecondsSinceEpoch(DateUtils.toSecondsSinceEpoch(now) + 3600),
			otherJWK.toPublicKey(),
			otherJWK.toPrivateKey());
		
		httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setClientX509Certificate(clientCert);
		httpRequest.setClientX509CertificateRootDN(clientCert.getIssuerDN().getName());
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		outputHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		assertEquals(httpRequest, outputHTTPRequest);
	}
	
	
	@Test
	public void testWithSoftwareStatement_missingJWTIssuer()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		
		var jwt = new SignedJWT(
			new JWSHeader.Builder(JWSAlgorithm.RS256)
				.keyID(PUBLISHER_JWK_RS256.getKeyID())
				.build(),
			new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
				.build());
		jwt.sign(new RSASSASigner(PUBLISHER_JWK_RS256.toPrivateKey()));
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		clientMetadata.setSoftwareStatement(jwt);
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		try {
			verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			fail();
		} catch (WrappedHTTPResponseException e) {
			var httpResponse = e.getHTTPResponse();
			assertEquals(400, httpResponse.getStatusCode());
			assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
			var errorObject = ErrorObject.parse(httpResponse);
			assertEquals(400, errorObject.getHTTPStatusCode());
			assertEquals(RegistrationError.INVALID_SOFTWARE_STATEMENT.getCode(), errorObject.getCode());
			assertEquals("Invalid software statement JWT: JWT missing required claims: [iss]", errorObject.getDescription());
			assertNull(errorObject.getURI());
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_additionalRequiredClaims()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.additionalRequiredClaims", "iss,iat,jti,software_id");
		System.setProperty("op.ssv.logClaims", "iss,iat,jti,software_id");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		statementMetadata.setSoftwareID(new SoftwareID("1781452a-6aaf-4ed6-a689-4fb1234429be"));
		
		var jwt = new SignedJWT(
			new JWSHeader.Builder(JWSAlgorithm.RS256)
				.keyID(PUBLISHER_JWK_RS256.getKeyID())
				.build(),
			new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
				.issuer(PUBLISHER_ISSUER.getValue())
				.issueTime(new Date())
				.jwtID("ae7549d7-f952-41ec-ba27-c1bcdb4277cf")
				.build());
		jwt.sign(new RSASSASigner(PUBLISHER_JWK_RS256.toPrivateKey()));
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		clientMetadata.setSoftwareStatement(jwt);
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		var rewrittenHTTPRequest = verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
		
		assertEquals(HTTPRequest.Method.POST, rewrittenHTTPRequest.getMethod());
		assertEquals(ContentType.APPLICATION_JSON.toString(), rewrittenHTTPRequest.getEntityContentType().toString());
		assertEquals(verifier.getConfiguration().registrationAccessToken.toAuthorizationHeader(), rewrittenHTTPRequest.getAuthorization());
		
		var mergedMetadata = OIDCClientMetadata.parse(rewrittenHTTPRequest.getBodyAsJSONObject());
		assertEquals(statementMetadata.getApplicationType(), mergedMetadata.getApplicationType());
		assertEquals(statementMetadata.getName(), mergedMetadata.getName());
		assertEquals(statementMetadata.getURI(), mergedMetadata.getURI());
		assertEquals(statementMetadata.getLogoURI(), mergedMetadata.getLogoURI());
		assertEquals(clientMetadata.getRedirectionURIs(), mergedMetadata.getRedirectionURIs());
		assertNull(mergedMetadata.getSoftwareStatement());
	}
	
	
	@Test
	public void testWithSoftwareStatement_additionalRequiredClaims_missingSoftwareID()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		System.setProperty("op.ssv.additionalRequiredClaims", "iss,iat,jti,software_id");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		
		var jwt = new SignedJWT(
			new JWSHeader.Builder(JWSAlgorithm.RS256)
				.keyID(PUBLISHER_JWK_RS256.getKeyID())
				.build(),
			new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
				.issuer(PUBLISHER_ISSUER.getValue())
				.issueTime(new Date())
				.jwtID("ae7549d7-f952-41ec-ba27-c1bcdb4277cf")
				.build());
		jwt.sign(new RSASSASigner(PUBLISHER_JWK_RS256.toPrivateKey()));
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		clientMetadata.setSoftwareStatement(jwt);
		
		var verifier = createConfiguredVerifier();
		
		assertEquals(Set.of("iss","iat","jti","software_id"), verifier.getConfiguration().additionalRequiredClaims);
		
		assertTrue(verifier.isEnabled());
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		try {
			verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			fail();
		} catch (WrappedHTTPResponseException e) {
			var httpResponse = e.getHTTPResponse();
			assertEquals(400, httpResponse.getStatusCode());
			assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
			var errorObject = ErrorObject.parse(httpResponse);
			assertEquals(400, errorObject.getHTTPStatusCode());
			assertEquals(RegistrationError.INVALID_SOFTWARE_STATEMENT.getCode(), errorObject.getCode());
			assertEquals("Missing required software statement JWT claim: software_id", errorObject.getDescription());
			assertNull(errorObject.getURI());
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_invalidSignature()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/jwks.json");
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		
		var jwt = new SignedJWT(
			new JWSHeader.Builder(JWSAlgorithm.RS256)
				.keyID(PUBLISHER_JWK_RS256.getKeyID())
				.build(),
			new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
				.issuer(PUBLISHER_ISSUER.getValue())
				.build());
		jwt.sign(new RSASSASigner(new RSAKeyGenerator(2048).generate())); // sign with non-published key
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		clientMetadata.setSoftwareStatement(jwt);
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		try {
			verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			fail();
		} catch (WrappedHTTPResponseException e) {
			var httpResponse = e.getHTTPResponse();
			assertEquals(400, httpResponse.getStatusCode());
			assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
			var errorObject = ErrorObject.parse(httpResponse);
			assertEquals(400, errorObject.getHTTPStatusCode());
			assertEquals(RegistrationError.INVALID_SOFTWARE_STATEMENT.getCode(), errorObject.getCode());
			assertEquals("Invalid software statement JWT: Signed JWT rejected: Invalid signature", errorObject.getDescription());
			assertNull(errorObject.getURI());
		}
	}
	
	
	@Test
	public void testWithSoftwareStatement_publisherJWKSetURL_404()
		throws Exception {
		
		System.setProperty("op.ssv.issuer", PUBLISHER_ISSUER.getValue());
		System.setProperty("op.ssv.issuerJWKSetURL", "http://localhost:" + port() + "/publisher/no-such-jwks.json"); // 404
		System.setProperty("op.ssv.jwsAlgorithms", "RS256,PS256,ES256");
		
		var statementMetadata = new OIDCClientMetadata();
		statementMetadata.setApplicationType(ApplicationType.WEB);
		statementMetadata.setName("Example client");
		statementMetadata.setURI(URI.create("https://client.example.com"));
		statementMetadata.setLogoURI(URI.create("https://client.example.com/logo.png"));
		
		var jwt = new SignedJWT(
			new JWSHeader.Builder(JWSAlgorithm.RS256)
				.keyID(PUBLISHER_JWK_RS256.getKeyID())
				.build(),
			new JWTClaimsSet.Builder(JWTClaimsSet.parse(statementMetadata.toJSONObject()))
				.issuer(PUBLISHER_ISSUER.getValue())
				.build());
		jwt.sign(new RSASSASigner(PUBLISHER_JWK_RS256));
		
		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setRedirectionURI(URI.create("https://client.example.com/cb"));
		clientMetadata.setSoftwareStatement(jwt);
		
		var verifier = createConfiguredVerifier();
		
		assertTrue(verifier.isEnabled());
		
		var httpRequest = new HTTPRequest(HTTPRequest.Method.POST, new URL("https:///"));
		httpRequest.setContentType(ContentType.APPLICATION_JSON.toString());
		httpRequest.setBody(clientMetadata.toString());
		
		try {
			verifier.interceptPostRequest(httpRequest, INTERCEPTOR_CTX);
			fail();
		} catch (WrappedHTTPResponseException e) {
			var httpResponse = e.getHTTPResponse();
			assertEquals(400, httpResponse.getStatusCode());
			assertEquals(ContentType.APPLICATION_JSON.toString(), httpResponse.getEntityContentType().toString());
			var errorObject = ErrorObject.parse(httpResponse);
			assertEquals(400, errorObject.getHTTPStatusCode());
			assertEquals(RegistrationError.INVALID_SOFTWARE_STATEMENT.getCode(), errorObject.getCode());
			assertEquals("Software statement JWT validation failed: Couldn't retrieve remote JWK set: http://localhost:" + port() + "/publisher/no-such-jwks.json", errorObject.getDescription());
			assertNull(errorObject.getURI());
		}
	}
}
