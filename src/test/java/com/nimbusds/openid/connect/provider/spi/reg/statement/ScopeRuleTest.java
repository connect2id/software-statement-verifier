package com.nimbusds.openid.connect.provider.spi.reg.statement;


import java.util.EnumSet;
import java.util.Set;

import static org.junit.Assert.*;

import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JsonOrgJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JsonOrgMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;
import org.junit.Before;
import org.junit.Test;

import com.nimbusds.oauth2.sdk.Scope;


public class ScopeRuleTest {
	
	
	@Before
	public void setUp() {
		
		com.jayway.jsonpath.Configuration.setDefaults(new com.jayway.jsonpath.Configuration.Defaults() {
			
			private final JsonProvider jsonProvider = new JsonOrgJsonProvider();
			private final MappingProvider mappingProvider = new JsonOrgMappingProvider();
			
			@Override
			public JsonProvider jsonProvider() {
				return jsonProvider;
			}
			
			@Override
			public MappingProvider mappingProvider() {
				return mappingProvider;
			}
			
			@Override
			public Set<Option> options() {
				return EnumSet.noneOf(Option.class);
			}
		});
	}

	
	@Test
	public void softwareRolesCase() {
		
		var scope = new Scope("openid", "accounts");
		var jsonPath = "$.software_roles[?(@=='AISP')]";
		
		var scopeRule = new ScopeRule(scope, jsonPath);
		
		var json =
			"{" +
			"    \"iss\": \"OpenBanking Ltd\"," +
			"    \"iat\": 1614268968," +
			"    \"jti\": \"aedcdc6c670b45fc\"," +
			"    \"software_environment\": \"sandbox\"," +
			"    \"software_mode\": \"Test\"," +
			"    \"software_redirect_uris\": [" +
			"      \"https://jsonplaceholder.typicode.com/\"" +
			"    ]," +
			"    \"software_roles\": [" +
			"      \"AISP\"," +
			"      \"PISP\"," +
			"      \"CBPII\"" +
			"    ]," +
			"    \"org_status\": \"Active\"," +
			"    \"org_name\": \"XYZ Bank\"," +
			"    \"org_contacts\": []" +
			"}";
		
		assertEquals(scope, scopeRule.getScope());
		assertEquals(jsonPath, scopeRule.getJSONPath());
		assertTrue(scopeRule.matches(json));
	}
	
	
	@Test
	public void noMatch() {
		
		var scope = new Scope("openid", "payments");
		var jsonPath = "$.software_roles[?(@=='PISP')]";
		
		var scopeRule = new ScopeRule(scope, jsonPath);
		
		var json =
			"{" +
			"    \"iss\": \"OpenBanking Ltd\"," +
			"    \"iat\": 1614268968," +
			"    \"jti\": \"aedcdc6c670b45fc\"," +
			"    \"software_environment\": \"sandbox\"," +
			"    \"software_redirect_uris\": [" +
			"      \"https://jsonplaceholder.typicode.com/\"" +
			"    ]," +
			"    \"software_roles\": [" +
			"      \"AISP\"," +
			"      \"CBPII\"" +
			"    ]," +
			"    \"org_status\": \"Active\"," +
			"    \"org_name\": \"XYZ Bank\"," +
			"    \"org_contacts\": []" +
			"}";
		
		assertEquals(scope, scopeRule.getScope());
		assertEquals(jsonPath, scopeRule.getJSONPath());
		assertFalse(scopeRule.matches(json));
	}
	
	
	@Test
	public void filter() {
		
		var scopeRuleAccounts = new ScopeRule(new Scope("openid", "accounts"), "$.software_roles[?(@=='AISP')]");
		var scopeRulePayments = new ScopeRule(new Scope("openid", "payments"), "$.software_roles[?(@=='PISP')]");
		
		var json =
			"{" +
			"    \"iss\": \"OpenBanking Ltd\"," +
			"    \"iat\": 1614268968," +
			"    \"jti\": \"aedcdc6c670b45fc\"," +
			"    \"software_environment\": \"sandbox\"," +
			"    \"software_mode\": \"Test\"," +
			"    \"software_redirect_uris\": [" +
			"      \"https://jsonplaceholder.typicode.com/\"" +
			"    ]," +
			"    \"software_roles\": [" +
			"      \"AISP\"," +
			"      \"PISP\"," +
			"      \"CBPII\"" +
			"    ]," +
			"    \"org_status\": \"Active\"," +
			"    \"org_name\": \"XYZ Bank\"," +
			"    \"org_contacts\": []" +
			"}";
		
		assertTrue(scopeRuleAccounts.matches(json));
		assertTrue(scopeRulePayments.matches(json));
		
		assertEquals(
			new Scope("openid", "accounts"),
			ScopeRule.filter(
				new Scope("openid", "accounts"),
				Set.of(scopeRuleAccounts,scopeRulePayments),
				json
			)
		);
	
		
		assertEquals(
			new Scope("openid", "accounts", "payments"),
			ScopeRule.filter(
				new Scope("openid", "accounts", "payments"),
				Set.of(scopeRuleAccounts, scopeRulePayments),
				json
			)
		);
	
		assertEquals(
			new Scope("openid", "accounts"),
			ScopeRule.filter(
				new Scope("openid", "accounts", "not-supported"),
				Set.of(scopeRuleAccounts, scopeRulePayments),
				json
			)
		);
	
		assertEquals(
			new Scope(),
			ScopeRule.filter(
				new Scope(),
				Set.of(scopeRuleAccounts, scopeRulePayments),
				json
			)
		);
	
		assertEquals(
			new Scope(),
			ScopeRule.filter(
				null,
				Set.of(scopeRuleAccounts, scopeRulePayments),
				json
			)
		);
	}
	
	
	@Test
	public void rejectNullScope() {
		
		try {
			new ScopeRule(null, "$.software_roles[?(@=='PISP')]");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The scope must not empty or null", e.getMessage());
		}
	}
	
	
	@Test
	public void rejectEmptyScope() {
		
		try {
			new ScopeRule(new Scope(), "$.software_roles[?(@=='PISP')]");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The scope must not empty or null", e.getMessage());
		}
	}
	
	
	@Test
	public void rejectNullJSONPath() {
		
		try {
			new ScopeRule(new Scope("openid"), null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The JSON path must not be blank or null", e.getMessage());
		}
	}
	
	
	@Test
	public void rejectEmptyJSONPath() {
		
		try {
			new ScopeRule(new Scope("openid"), "");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The JSON path must not be blank or null", e.getMessage());
		}
	}
	
	
	@Test
	public void rejectBlankJSONPath() {
		
		try {
			new ScopeRule(new Scope("openid"), " ");
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The JSON path must not be blank or null", e.getMessage());
		}
	}
}
