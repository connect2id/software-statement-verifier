package com.nimbusds.openid.connect.provider.spi.reg.statement;


import static org.junit.Assert.assertEquals;

import org.junit.Test;


public class RequestTypeTest {


	@Test
	public void testConstants() {
		
		
		assertEquals("JSON", RequestType.JSON.name());
		assertEquals("JWT", RequestType.JWT.name());
		assertEquals(2, RequestType.values().length);
	}
}
