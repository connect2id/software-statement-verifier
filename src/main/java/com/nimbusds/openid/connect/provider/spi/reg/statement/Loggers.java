package com.nimbusds.openid.connect.provider.spi.reg.statement;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Log4j loggers.
 */
final class Loggers {


	/**
	 * The main logger. Records general configuration, startup, shutdown
	 * and system messages.
	 */
	public static final Logger MAIN = LogManager.getLogger("MAIN");
	
	
	/**
	 * OAuth 2.0 client registration endpoint logger.
	 */
	public static final Logger REGISTRATION = LogManager.getLogger("CLIENT-REG");
	
}
