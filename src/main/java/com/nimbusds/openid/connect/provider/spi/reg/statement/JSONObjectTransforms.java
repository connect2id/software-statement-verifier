package com.nimbusds.openid.connect.provider.spi.reg.statement;


import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;


/**
 * JSON object transforms.
 */
public class JSONObjectTransforms {
	
	
	/**
	 * Removes a JSON object member.
	 *
	 * @param jsonObject The JSON object.
	 * @param memberName The member name.
	 */
	public static void remove(final JSONObject jsonObject, final String memberName) {
		
		if (! jsonObject.containsKey(memberName)) {
			return;
		}
		
		jsonObject.remove(memberName);
		
		Loggers.REGISTRATION.debug("[SSV3002] JSON object transform: Removed: {}", memberName);
	}
	
	
	/**
	 * Renames a JSON object member.
	 *
	 * @param jsonObject    The JSON object member.
	 * @param memberName    The member name.
	 * @param newMemberName The new member name.
	 */
	public static void rename(final JSONObject jsonObject, final String memberName, final String newMemberName) {
		
		if (! jsonObject.containsKey(memberName)) {
			return;
		}
		
		final Object memberValue = jsonObject.remove(memberName);
		
		jsonObject.put(newMemberName, memberValue);
		
		Loggers.REGISTRATION.debug("[SSV3000] JSON object transform: Renamed: {}", memberName);
	}
	
	
	/**
	 * Moves a JSON object member into the "data" member.
	 *
	 * @param jsonObject The JSON object.
	 * @param memberName The member name.
	 */
	public static void moveIntoData(final JSONObject jsonObject, final String memberName) {
		
		if (! jsonObject.containsKey(memberName)) {
			return;
		}
		
		final Object memberValue = jsonObject.remove(memberName);
		
		JSONObject data;
		try {
			data = JSONObjectUtils.getJSONObject(jsonObject, "data");
		} catch (ParseException e) {
			data = new JSONObject();
		}
		
		data.put(memberName, memberValue);
		
		jsonObject.put("data", data);
		
		Loggers.REGISTRATION.debug("[SSV3001] JSON object transform: Moved into data: {}", memberName);
	}
}
