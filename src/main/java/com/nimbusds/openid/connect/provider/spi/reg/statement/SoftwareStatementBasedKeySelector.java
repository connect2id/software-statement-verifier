package com.nimbusds.openid.connect.provider.spi.reg.statement;


import java.net.MalformedURLException;
import java.net.URL;
import java.security.Key;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.KeySourceException;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.jwk.source.RemoteJWKSet;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.util.ResourceRetriever;
import com.nimbusds.jwt.JWTClaimsSet;


/**
 * Key selector for verifying registration request JWTs.
 */
class SoftwareStatementBasedKeySelector implements JWSKeySelector<SoftwareStatementContext> {
	
	
	/**
	 * The HTTP resource retriever.
	 */
	private final ResourceRetriever resourceRetriever;
	
	
	/**
	 * The statement specific URL claim name.
	 */
	private final String urlClaimName;
	
	
	/**
	 * Creates a new key selector for verifying registration request JWTs.
	 *
	 * @param urlClaimName      The URL claim name. Must not be
	 *                          {@code null}.
	 * @param resourceRetriever The HTTP resource retriever to use. Must
	 *                          not be {@code null}.
	 */
	public SoftwareStatementBasedKeySelector(final String urlClaimName,
						 final ResourceRetriever resourceRetriever) {
		
		if (urlClaimName == null) {
			throw new IllegalArgumentException("The URL claim name must not be null");
		}
		
		this.urlClaimName = urlClaimName;
		
		if (resourceRetriever == null) {
			throw new IllegalArgumentException("The resource retriever must not be null");
		}
		
		this.resourceRetriever = resourceRetriever;
	}
	
	
	/**
	 * Resolves the JWK set URL from the specified software statement
	 * claims set.
	 *
	 * @param statementClaims The software statement claims set.
	 *
	 * @return The resolved URL, {@code null} if none.
	 */
	URL resolveJWKSetURL(final JWTClaimsSet statementClaims) {
		
		URL url;
		try {
			var uri = statementClaims.getURIClaim(urlClaimName);
			
			if (uri == null) {
				Loggers.REGISTRATION.info("[SSV0200] Missing software statement {}", urlClaimName);
				return null;
			}
			
			if (! uri.isAbsolute()) {
				Loggers.REGISTRATION.info("[SSV0203] Software statement {} not an absolute URL", uri);
				return null;
			}
			
			url = uri.toURL();
			
		} catch (ParseException | MalformedURLException e) {
			Loggers.REGISTRATION.info("[SSV0201] Illegal software statement {}: {}", urlClaimName, e.getMessage());
			return null;
		}
		
		Loggers.REGISTRATION.info("[SSV0202] Resolved JWK set URL for the registration JWT validation: {}", url);
		
		return url;
	}
	
	
	/**
	 * Creates a new JWK matcher for the specified JWS header.
	 *
	 * @param header The JWS header. Must not be {@code null}.
	 *
	 * @return The JWK matcher.
	 */
	static JWKMatcher createJWKMatcher(final JWSHeader header) {
		
		return new JWKMatcher.Builder()
			.keyUses(KeyUse.SIGNATURE, null)
			.keyID(header.getKeyID())
			.build();
	}
	
	
	@Override
	public List<Key> selectJWSKeys(final JWSHeader header,
				       final SoftwareStatementContext ctx)
		throws KeySourceException {
		
		URL resolvedURL = resolveJWKSetURL(ctx.getStatementClaims());
		
		if (resolvedURL == null) {
			return Collections.emptyList();
		}
		
		List<JWK> candidates = new RemoteJWKSet<SoftwareStatementContext>(
			resolvedURL,
			resourceRetriever)
			.get(new JWKSelector(createJWKMatcher(header)), null);
		
		Loggers.REGISTRATION.trace("[SSV0204] Retrieved registration JWT signing JWK candidates: {}", candidates);
		
		return KeyConverter.toJavaKeys(candidates);
	}
}
