package com.nimbusds.openid.connect.provider.spi.reg.statement;


import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Set;

import com.nimbusds.oauth2.sdk.util.StringUtils;


/**
 * JWK set source specified by URI.
 */
final class JWKSetSource {
	
	
	private final URL url;
	
	
	private final String urlClaimName;
	
	
	/**
	 * Creates a new JWK set source.
	 *
	 * @param uri The JWK set source URI. Must not be {@code null}.
	 *
	 * @throws URISyntaxException If the URI isn't accepted.
	 */
	public JWKSetSource(final URI uri)
		throws URISyntaxException {
		
		if (uri.getScheme() == null) {
			throw new URISyntaxException(uri.toString(), "Missing URI scheme");
		}
		
		if (Set.of("https", "http").contains(uri.getScheme())) {
			
			try {
				this.url = uri.toURL();
			} catch (MalformedURLException e) {
				throw new URISyntaxException(uri.toString(), e.getMessage());
			}
			
			urlClaimName = null;
			
		} else if ("urn".equals(uri.getScheme())) {
			
			if (uri.getSchemeSpecificPart() == null || ! uri.getSchemeSpecificPart().startsWith("software_statement:")) {
				throw new URISyntaxException(uri.toString(), "The URN must start with urn:software_statement:");
			}
			
			this.urlClaimName = uri.getSchemeSpecificPart().substring("software_statement:".length());
			
			if (StringUtils.isBlank(urlClaimName)) {
				throw new URISyntaxException(uri.toString(), "Missing claim name after urn:software_statement:");
			}
			
			url = null;
			
		} else {
			throw new URISyntaxException(uri.toString(), "The URI scheme must be https, http or urn");
		}
	}
	
	
	/**
	 * Returns the static URL of the JWK set source.
	 *
	 * @return The URL, {@code null} if a {@link #getURLClaimName()
	 *         software statement claim is specified instead}.
	 */
	public URL getStaticURL() {
		return url;
	}
	
	
	/**
	 * Returns the software statement URL claim name of the JWK set source.
	 *
	 * @return The claim name, {@code null} if a {@link #getStaticURL()
	 *         static URL is specified instead}.
	 */
	public String getURLClaimName() {
		return urlClaimName;
	}
	
	
	@Override
	public String toString() {
		if (getStaticURL() != null) {
			return getStaticURL().toString();
		} else {
			return "urn:software_statement:" + getURLClaimName();
		}
	}
}
