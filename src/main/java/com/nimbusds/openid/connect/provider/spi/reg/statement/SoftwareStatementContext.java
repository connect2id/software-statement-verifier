package com.nimbusds.openid.connect.provider.spi.reg.statement;


import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;


final class SoftwareStatementContext implements SecurityContext {


	private final JWTClaimsSet statementClaims;
	
	
	public SoftwareStatementContext(final JWTClaimsSet statementClaims) {
		if (statementClaims == null) {
			throw new IllegalArgumentException();
		}
		this.statementClaims = statementClaims;
	}
	
	
	public JWTClaimsSet getStatementClaims() {
		return statementClaims;
	}
}
