package com.nimbusds.openid.connect.provider.spi.reg.statement;


/**
 * Request type.
 */
enum RequestType {
	
	/**
	 * The request is a JSON entity.
	 */
	JSON,
	
	
	/**
	 * The request is a signed JWT entity.
	 */
	JWT
}
