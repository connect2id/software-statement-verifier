# Software Statement Verifier for the Connect2id server

Copyright (c) Connect2idLtd., 2020 - 2024


Software statement verifier for the [client registration 
endpoint](https://connect2id.com/products/server/docs/api/client-registration). 
Implements the 
[RegistrationInterceptor](https://www.javadoc.io/static/com.nimbusds/c2id-server-sdk/4.30/com/nimbusds/openid/connect/provider/spi/reg/RegistrationInterceptor.html)
SPI from the Connect2id server SDK.

* Requires [Connect2id server](https://connect2id.com/products/server) v11.0+.

* Verifies software statements in HTTP POST client registration requests and 
  merges them into the client metadata to produce a new rewritten HTTP POST 
  request, which is authorised with a configured registration access token 
  ([op.reg.apiAccessTokenSHA256](https://connect2id.com/products/server/docs/config/core#op-reg-apiAccessTokenSHA256))
  for the underlying Connect2id server client registration endpoint.
  
* HTTP POST client registration requests without a software statement are 
  passed unmodified, to allow registration requests authorised with a token. If 
  the Connect2id server is not configured for open registration 
  ([op.reg.allowOpenRegistration](https://connect2id.com/products/server/docs/config/core#op-reg-allowOpenRegistration)) 
  and an initial registration token isn't included in the original request it 
  will be rejected downstream.
  
* HTTP POST client registration requests with an invalid software statement
  will get rejected with an HTTP 400 Invalid Request status code and the 
  "invalid_software_statement" error code.
  
* The software statement signatures are verified against a public JWK set 
  published at a configured URL.

* Supported software statement JWS algorithms: RS256, RS384, RS512, PS256, 
  PS384, PS512, ES256, ES384 and ES512.
  
* The registration request may be additionally encoded into a signed JWT and 
  submitted over mutual TLS with a client X.509 certificate, to conform with 
  Open Banking and other profiles.
  
* Any "scope" values that are not asserted by the software statement will be 
  scrubbed from the client metadata, unless one or more rules for allowing 
  selected scope values are configured.

* Any custom (non-standard) fields that are not asserted by the software 
  statement will be scrubbed from the client metadata. This is done because
  custom fields like "preferred_client_id" and "preferred_client_secret" can 
  have a special meaning to the Connect2id server in an authorised registration 
  request.


## Maven

```xml
<dependency>
  <groupId>com.nimbusds</groupId>
  <artifactId>software-statement-verifier</artifactId>
  <version>[version]</version>
</dependency>
```


## Configuration

The software statement verifier is configured from Java system properties or 
from properties in the optional `/WEB-INF/softwareStatementVerifier.properties` 
file. The Java system properties have precedence. If no configuration is found 
the verifier is disabled and received HTTP POST requests will be passed through 
with no modification.


### Basic configuration

Basic configuration for processing optional software statements in client 
registration requests.

* `op.ssv.enable` -- Enables / disables the HTTP claims source. Disabled by 
  default.
  
* `op.ssv.issuer` -- The accepted "iss" (issuer) claim of the software 
  statements.
  
* `op.ssv.issuerJWKSetURL` -- The URL of the public JWK set for verifying the 
  software statement signatures.
  
* `op.ssv.jwsAlgorithms` -- The accepted JWS algorithms for the software 
  statements, as comma and / or space separated list.

* `op.ssv.jwtTypes` -- The accepted "typ" (type) JWT header values of the 
  software statements, as comma and / or space separated list. If blank or 
  omitted the JWT type checking will accept software statement with no "typ"
  header or the generic header value "JWT". 
  
* `op.ssv.connectTimeout` -- The timeout in milliseconds for establishing HTTP 
  connections, such as those to retrieve the JWK set. If zero the underlying 
  HTTP client library will determine the timeout.
  
* `op.ssv.readTimeout` -- The timeout in milliseconds for obtaining HTTP 
  responses after connection. If zero the underlying HTTP client library will 
  determine the timeout.
  
* `op.ssv.registrationAccessToken` -- Access token of type bearer 
  (non-expiring) for accessing the client registration endpoint. Client 
  registration HTTP POST requests with a merged valid software statement will 
  have their `Authorization` header set to this token, thus authorising the 
  request at the underlying registration endpoint. See the Connect2id server 
  [op.reg.apiAccessTokenSHA256](https://connect2id.com/products/server/docs/config/core#op-reg-apiAccessTokenSHA256) 
  configuration property.
  
* `op.ssv.additionalRequiredClaims` -- The names of additional claims that must 
  be present in the software statement other than the "iss" (issuer) claim, as 
  comma and / or space separated list. If blank or omitted the only required 
  claim is the standard "iss" (issuer) claim.
  
* `op.ssv.logClaims` -- Names of software statement claims to log at INFO level
  under `SSV0100`, as comma and / or space separated list. If blank or omitted 
  only the standard "iss" (issuer) claim will be logged.


### Optional client X.509 certificate configuration

For registration profiles which require mutual TLS with a client certificate in
addition to the software statement.
  
* `op.ssv.clientX509Certificate.require` -- If `true` the HTTP POST request 
  must include a client X.509 certificate validated by the TLS terminator / 
  reverse proxy. The default value if blank or omitted is `false`.

* `op.ssv.clientX509Certificate.rootDN` -- The required root DN (distinguished 
  name) of the client X.509 certificates, if client certificates are required. 
  If blank or omitted the certificate root DN is not checked.


### Optional request JWT configuration

For registration profiles which require the registration request to be a signed 
JWT.

* `op.ssv.requestType` -- The accepted HTTP POST request type. The default 
  value if blank or omitted is `JSON`.
    * `JSON` -- The request entity content type must be `application/json`. 
      This is the standard type, according to the OAuth 2.0 Dynamic Client 
      Registration Protocol (RFC7591). 
    * `JWT` -- The request entity content type must be `application/jwt` and 
      the JWT must be signed with an accepted JWS algorithm.
      
* `op.ssv.requestJWT.jwkSetSource` -- The URI of the public JWK set for 
  verifying request JWT signatures (not the software statements!). Supported 
  URI types:
    * `https` or `http` URL of the JWK set.
    * URN with in the form of `urn:software_statement:[url-claim-name]` where
      `[urn-claim-name]` is the name of a software statement claim with a 
      `https` or `http` URL of the JWK set to use. If a registration request
      JWT is to be validated successfully in this way it must include a valid
      signed software statement.
      
* `op.ssv.requestJWT.jwsAlgorithms` -- The accepted JWS algorithms for the 
  registration request JWT, as comma and / or space separated list.
  
* `op.ssv.requestJWT.requiredClaims` -- The names of claims that must be 
  present in a registration request JWT, as comma and / or space separated 
  list, if blank or omitted none. If the "aud" (audience) claim is included it 
  will be matched against the Connect2id server issuer URL. If the "exp" 
  (expiration) claim is included it will be checked accordingly.


### Optional scope rules

If the software statement contains the "scope" claim its scope values will be
accepted into the client metadata. The top-level "scope" client metadata field 
(outside the statement) will be removed, unless there are one or more rules
configured to allow selected scope values to pass.

To add one or more such rules use the following configuration pattern where 
`[rule-name]` is a unique string identifying the rule, e.g. `r1`:

* `op.ssv.scopeRules.[rule-name].scope` -- Scope value(s) to allow in the
  top-level "scope" client metadata field. If multiple scope values are defined 
  they must be space separated.
  
* `op.ssv.scopeRules.[rule-name].jsonPath` -- JSON Path query for the software
  statement claims JSON. If the query matches and returns one or more filtered
  items for a software statement the configured scope values for the rule will 
  be allowed to pass if set in the top-level "scope" client metadata field.
  The JSON Path syntax is interpreted with [Jayway
  JsonPath](https://github.com/json-path/JsonPath). Note, when passing this 
  configuration property via the shell special characters in the JSON Path 
  query may need to be escaped.
  
Check the examples below for usage hints. To quickly evaluate JSON paths 
online go to [jsonpath.com](https://jsonpath.com/).


### Optional client metadata transformations

For transforming the client metadata after the software statement is applied to 
it. The transformations will be applied in the following order: remove, rename, 
move.

* `op.ssv.transforms.remove` -- Names of top-level JSON object members to be 
  removed from the client metadata, as comma and / or space separated list. If 
  blank or omitted no members will be removed. Can be used to scrub software 
  statement and request JWT claims which are intended for the JWT verification 
  only and should not be passed on with the client registration request.
  
* `op.ssv.transforms.rename.*` -- Map of names of top-level JSON object members 
  to be renamed in the client metadata. For example, the 
  `op.ssv.transforms.rename.software_jwks_endpoint=jwks_uri` configuration 
  property will cause `software_jwks_endpoint` to be renamed as `jwks_uri` and
  thus be picked as a standard OAuth client registration parameter.
  
* `op.ssv.transforms.moveIntoData` -- Names of top-level JSON object members in 
  the client metadata to be moved into the "data" JSON object member, as comma
  and / or space separated list. If blank or omitted no members will be moved. 
  The Connect2id server uses the custom "data" client metadata parameter (of 
  type JSON object) to enable storing of additional non-standard parameters for
  the client. Top-level parameters which are not recognised will be scrubbed 
  from the client registration.



Example minimal configuration for processing software statements from some 
issuer:

```ini
op.ssv.enable=true
op.ssv.issuer=https://publisher.example.com
op.ssv.issuerJWKSetURL=https://publisher.example.com/jwks.json
op.ssv.jwsAlgorithms=RS256,PS256
op.ssv.connectTimeout=1000
op.ssv.readTimeout=1000
op.ssv.registrationAccessToken=ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
```

Example configuration which requires the software statement to be signed with 
the ES256 JWS algorithm and to require the `iat`, `software_id`, 
`redirect_uris` and `grant_types` claims to be present in it (apart from the
`iss` (issuer) claim mandated by RFC 7591). Those claims will also be logged at
INFO level for each validated request.

```ini
op.ssv.enable=true
op.ssv.issuer=https://publisher.example.com
op.ssv.issuerJWKSetURL=https://publisher.example.com/jwks.json
op.ssv.jwsAlgorithms=ES256
op.ssv.connectTimeout=1000
op.ssv.readTimeout=1000
op.ssv.registrationAccessToken=ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
op.ssv.additionalRequiredClaims=iat,software_id,redirect_uris,grant_types
op.ssv.logClaims=iat,software_id,redirect_uris,grant_types
```

Example configuration to allow the top-level client metadata "scope" values 
`openid` and `accounts` when the software statement contains a 
`software_roles` JSON array with an `AISP` item. Similarly, to allow the 
"scope" values `openid` and `payments` for a `PISP` item. The 
`op.ssv.transforms.remove` setting removes the merged `software_roles` from the
final client metadata.

```ini
op.ssv.enable=true
op.ssv.issuer=https://publisher.example.com
op.ssv.issuerJWKSetURL=https://publisher.example.com/jwks.json
op.ssv.jwsAlgorithms=RS256,PS256
op.ssv.connectTimeout=1000
op.ssv.readTimeout=1000
op.ssv.registrationAccessToken=ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
op.ssv.scopeRules.1.scope=openid accounts
op.ssv.scopeRules.1.jsonPath=$.software_roles[?(@=='AISP')]
op.ssv.scopeRules.2.scope=openid payments
op.ssv.scopeRules.2.jsonPath=$.software_roles[?(@=='PISP')]
op.ssv.transforms.remove=software_roles
```


## Change log

* 1.0 (2020-11-30)
    * First official release.

* 1.1 (2020-11-30)
    * Adds optional op.softwareStatements.additionalRequiredClaims 
      configuration.
    * Adds optional op.softwareStatements.requireClientX509Certificate 
      configuration.

* 1.2 (2020-12-02)
    * Adds support for client X.509 certificate checking and registration 
      requests encoded into a signed JWT, conforming to the Open Banking 
      profile.

* 2.0 (2020-12-04)
    * Adds support for configuring JSON object transformations on the merged
      client metadata.
    * Adds optional op.ssv.logClaims configuration.
    * Shortens the configuration property prefix to op.ssv.*

* 2.0.1 (2020-12-07)
    * Fixes initialisation when the verifier is disabled.
  
* 2.0.2 (2020-12-07)
    * Logs configuration in init method.

* 2.0.3 (2020-12-07)
    * Logs client X.509 certificate issuer and subject DNs under SSV0113.

* 2.1 (2020-12-21)
    * The "scope" field and all custom (non-standard) fields are being scrubbed
      from top-level (not software statement) client metadata as those fields, 
      including "preferred_client_id" and "preferred_client_secret" can have 
      a special meaning to the Connect2id server in an authorised registration
      request.

* 2.1.1 (2021-02-17)
    * Bumps the SDK dependencies for Connect2id server 11.0+ (shaded JSON Smart
      in the Nimbus JOSE+JWT library).

* 2.2 (2021-03-22)
    * Adds new configuration for scope rules based on JSON Path expressions 
      (iss #1).
    * Updates dependencies. 

* 2.2.1 (2021-04-14)
    * Bumps dependencies.

* 2.2.2 (2021-12-16)
    * Bumps dependencies.

* 2.2.3 (2023-04-27)
    * Bumps dependencies, in particular org.json:json to 20230227.

* 2.2.4 (2023-04-27)
    * Bumps OIDC SDK to 10.8.
    
* 2.2.5 (2023-05-09)
    * Bumps JSON path to 2.8.0.    
    
* 2.2.6 (2023-10-17)
    * Bumps org.json:json to 20231013.

* 2.3 (2024-08-20)
    * Upgrades build to Java 17 (for Connect2id server 15.0+)
    * Adds new optional opj.ssv.jwtTypes configuration property.
    * Bumps deps for Connect2id server 15.0+
    * Bumps OIDC SDK to 11.18
    * Bumps Connect2id server SDK to 5.1
    * Bumps NimbusDS Common to 3.0.3
    * Bumps JSON Smart to 2.5.1


Questions or comments? Email support@connect2id.com
